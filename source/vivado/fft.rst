
Vivado
******

Fast Fourier transform
======================


Below tcl scripts uses the DDS compiler to generate the input sine wave; which is used by FFT IP. Finally absolute value of the output is calculated in the design. 


.. code-block:: tcl
    :linenos:
    :caption: FFT Design


    # Vivado 2019.1
    # Ultra96 board


    create_project fft_test ./fft_test -part xczu3eg-sbva484-1-e
    set_property board_part em.avnet.com:ultra96:part0:1.2 [current_project]

    create_bd_design "fft1"
    update_compile_order -fileset sources_1


    # DDS compiler 
    create_bd_cell -type ip -vlnv xilinx.com:ip:dds_compiler:6.0 dds_compiler_0
    set_property -dict [list CONFIG.PartsPresent {Phase_Generator_and_SIN_COS_LUT} CONFIG.Parameter_Entry {System_Parameters} CONFIG.Spurious_Free_Dynamic_Range {90} CONFIG.Output_Selection {Sine} CONFIG.Has_Phase_Out {false} CONFIG.Output_Frequency1 {2.56} CONFIG.Frequency_Resolution {0.4} CONFIG.Noise_Shaping {Auto} CONFIG.Phase_Width {28} CONFIG.Output_Width {15} CONFIG.DATA_Has_TLAST {Not_Required} CONFIG.S_PHASE_Has_TUSER {Not_Required} CONFIG.M_DATA_Has_TUSER {Not_Required} CONFIG.Latency {8} CONFIG.PINC1 {11010001101101110001011}] [get_bd_cells dds_compiler_0]


    # FFT
    create_bd_cell -type ip -vlnv xilinx.com:ip:xfft:9.1 xfft_0
    set_property -dict [list CONFIG.data_format.VALUE_SRC USER CONFIG.input_width.VALUE_SRC USER] [get_bd_cells xfft_0]
    set_property -dict [list CONFIG.transform_length {16384} CONFIG.target_clock_frequency {100} CONFIG.implementation_options {pipelined_streaming_io} CONFIG.target_data_throughput {100} CONFIG.input_width {8} CONFIG.xk_index {false} CONFIG.implementation_options {pipelined_streaming_io} CONFIG.number_of_stages_using_block_ram_for_data_and_phase_factors {7}] [get_bd_cells xfft_0]


    # Simulation clock
    create_bd_cell -type ip -vlnv xilinx.com:ip:sim_clk_gen:1.0 sim_clk_gen_0


    # connections
    connect_bd_net [get_bd_pins sim_clk_gen_0/clk] [get_bd_pins dds_compiler_0/aclk]
    connect_bd_net [get_bd_pins xfft_0/aclk] [get_bd_pins sim_clk_gen_0/clk]
    connect_bd_net [get_bd_pins dds_compiler_0/m_axis_data_tdata] [get_bd_pins xfft_0/s_axis_data_tdata]
    connect_bd_net [get_bd_pins dds_compiler_0/m_axis_data_tvalid] [get_bd_pins xfft_0/s_axis_data_tvalid]


    # Slice Re and Im
    create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_0
    set_property -dict [list CONFIG.DIN_FROM {7} CONFIG.DIN_WIDTH {16} CONFIG.DIN_TO {0} CONFIG.DOUT_WIDTH {8}] [get_bd_cells xlslice_0]
    set_property name xlslice_Re [get_bd_cells xlslice_0]

    create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_0
    set_property -dict [list CONFIG.DIN_TO {8} CONFIG.DIN_FROM {15} CONFIG.DIN_WIDTH {16} CONFIG.DIN_FROM {15} CONFIG.DOUT_WIDTH {8}] [get_bd_cells xlslice_0]
    set_property name xlslice_Im [get_bd_cells xlslice_0]

    connect_bd_net [get_bd_pins xfft_0/m_axis_data_tdata] [get_bd_pins xlslice_Im/Din]
    connect_bd_net [get_bd_pins xlslice_Re/Din] [get_bd_pins xfft_0/m_axis_data_tdata]


    # Multipler and adder to caculate the absolute value of or complex number
    create_bd_cell -type ip -vlnv xilinx.com:ip:mult_gen:12.0 mult_gen_0
    set_property -dict [list CONFIG.PortAType.VALUE_SRC USER CONFIG.PortAWidth.VALUE_SRC USER CONFIG.PortBType.VALUE_SRC USER CONFIG.PortBWidth.VALUE_SRC USER] [get_bd_cells mult_gen_0]
    set_property -dict [list CONFIG.PortAWidth {8} CONFIG.PortBWidth {8} CONFIG.OutputWidthHigh {15} CONFIG.PipeStages {0}] [get_bd_cells mult_gen_0]

    copy_bd_objs /  [get_bd_cells {mult_gen_0}]
    set_property location {2 296 -56} [get_bd_cells mult_gen_1]

    connect_bd_net [get_bd_pins xlslice_Im/Dout] [get_bd_pins mult_gen_0/A]
    connect_bd_net [get_bd_pins mult_gen_0/B] [get_bd_pins xlslice_Im/Dout]
    connect_bd_net [get_bd_pins xlslice_Re/Dout] [get_bd_pins mult_gen_1/A]
    connect_bd_net [get_bd_pins mult_gen_1/B] [get_bd_pins xlslice_Re/Dout]

    create_bd_cell -type ip -vlnv xilinx.com:ip:c_addsub:12.0 c_addsub_0
    set_property -dict [list CONFIG.A_Type.VALUE_SRC USER CONFIG.B_Type.VALUE_SRC USER CONFIG.A_Width.VALUE_SRC USER CONFIG.B_Width.VALUE_SRC USER] [get_bd_cells c_addsub_0]
    set_property -dict [list CONFIG.A_Width {16} CONFIG.B_Width {16} CONFIG.Latency {0} CONFIG.CE {false} CONFIG.Out_Width {16} CONFIG.B_Value {0000000000000000}] [get_bd_cells c_addsub_0]
    connect_bd_net [get_bd_pins mult_gen_0/P] [get_bd_pins c_addsub_0/A]
    connect_bd_net [get_bd_pins mult_gen_1/P] [get_bd_pins c_addsub_0/B]

    # create output ports
    make_bd_pins_external  [get_bd_pins c_addsub_0/S]
    set_property name abs_fft [get_bd_ports S_0]

    create_bd_port -dir O -from 15 -to 0 -type data dds_output
    connect_bd_net [get_bd_ports dds_output] [get_bd_pins dds_compiler_0/m_axis_data_tdata]
    create_bd_port -dir O -type data dds_tvalid
    connect_bd_net [get_bd_ports dds_tvalid] [get_bd_pins dds_compiler_0/m_axis_data_tvalid]


    make_bd_pins_external  [get_bd_pins xfft_0/m_axis_data_tvalid]
    set_property name fft_tvalid [get_bd_ports m_axis_data_tvalid_0]

    # add xk_index to FFT
    set_property -dict [list CONFIG.xk_index {true}] [get_bd_cells xfft_0]
    make_bd_pins_external  [get_bd_pins xfft_0/m_axis_data_tuser]
    set_property name fft_xk_index [get_bd_ports m_axis_data_tuser_0]


    regenerate_bd_layout
    validate_bd_design
    save_bd_design


    make_wrapper -files [get_files ./fft_test/fft_test.srcs/sources_1/bd/fft1/fft1.bd] -top
    add_files -norecurse ./fft_test/fft_test.srcs/sources_1/bd/fft1/hdl/fft1_wrapper.v




    ## Now run simulation 

    ## Next change the output order to natural_order (using below two commands) and rerun the simulation 

    # set_property -dict [list CONFIG.output_ordering {natural_order}] [get_bd_cells xfft_0]
    # save_bd_design


* Below design will be created by above script, 

.. _`fig_vivado1`:

.. figure:: vivado/vivado1.jpg
   :width: 100%

   FFT design


* Now, run the simulation and we will get the below result. 

.. _`fig_vivado2`:

.. figure:: vivado/vivado2.jpg
   :width: 100%

   Result for output as bit-reverse order


* Above tcl script is created by bit-reverse output order. Run the below command in the tcl-console to change it to natural order output, 

.. code-block:: tcl

    set_property -dict [list CONFIG.output_ordering {natural_order}] [get_bd_cells xfft_0]
    save_bd_design

* Run the simulation again and we will get the below output. Note that, the xk_index is in sequence now, 


.. _`fig_vivado3`:

.. figure:: vivado/vivado3.jpg
   :width: 100%

   Result for Natural order output 



FFT testbench
=============


.. code-block:: tcl

    # Vivado 2019.1
    # Ultra96

    create_project FFT_test2 ./fft/fft_sim/FFT_test2 -part xczu3eg-sbva484-1-e -force
    set_property board_part em.avnet.com:ultra96:part0:1.2 [current_project]


    # # FFT IP
    # create_ip -name xfft -vendor xilinx.com -library ip -version 9.1 -module_name xfft_0
    # set_property -dict [list CONFIG.transform_length {128} CONFIG.target_clock_frequency {200} CONFIG.target_data_throughput {200} CONFIG.complex_mult_type {use_mults_performance} CONFIG.butterfly_type {use_xtremedsp_slices} CONFIG.implementation_options {automatically_select} CONFIG.number_of_stages_using_block_ram_for_data_and_phase_factors {0}] [get_ips xfft_0]
    # generate_target {instantiation_template} [get_files ./fft/fft_sim/FFT_test2/FFT_test2.srcs/sources_1/ip/xfft_0/xfft_0.xci]
    # update_compile_order -fileset sources_1
    # generate_target all [get_files  ./fft/fft_sim/FFT_test2/FFT_test2.srcs/sources_1/ip/xfft_0/xfft_0.xci]
    # catch { config_ip_cache -export [get_ips -all xfft_0] }
    # export_ip_user_files -of_objects [get_files ./fft/fft_sim/FFT_test2/FFT_test2.srcs/sources_1/ip/xfft_0/xfft_0.xci] -no_script -sync -force -quiet
    # create_ip_run [get_files -of_objects [get_fileset sources_1] ./fft/fft_sim/FFT_test2/FFT_test2.srcs/sources_1/ip/xfft_0/xfft_0.xci]
    # launch_runs -jobs 8 xfft_0_synth_1
    # export_simulation -of_objects [get_files ./fft/fft_sim/FFT_test2/FFT_test2.srcs/sources_1/ip/xfft_0/xfft_0.xci] -directory ./fft/fft_sim/FFT_test2/FFT_test2.ip_user_files/sim_scripts -ip_user_files_dir ./fft/fft_sim/FFT_test2/FFT_test2.ip_user_files -ipstatic_source_dir ./fft/fft_sim/FFT_test2/FFT_test2.ip_user_files/ipstatic -lib_map_path [list {modelsim=./fft/fft_sim/FFT_test2/FFT_test2.cache/compile_simlib/modelsim} {questa=./fft/fft_sim/FFT_test2/FFT_test2.cache/compile_simlib/questa} {ies=./fft/fft_sim/FFT_test2/FFT_test2.cache/compile_simlib/ies} {xcelium=./fft/fft_sim/FFT_test2/FFT_test2.cache/compile_simlib/xcelium} {vcs=./fft/fft_sim/FFT_test2/FFT_test2.cache/compile_simlib/vcs} {riviera=./fft/fft_sim/FFT_test2/FFT_test2.cache/compile_simlib/riviera}] -use_ip_compiled_libs -force -quiet


    # create block design
    create_bd_design "design_1"
    create_bd_cell -type ip -vlnv xilinx.com:ip:xfft:9.1 xfft_0
    set_property -dict [list CONFIG.transform_length {128} CONFIG.target_clock_frequency {200} CONFIG.target_data_throughput {200} CONFIG.complex_mult_type {use_mults_performance} CONFIG.butterfly_type {use_xtremedsp_slices} CONFIG.implementation_options {automatically_select} CONFIG.number_of_stages_using_block_ram_for_data_and_phase_factors {0}] [get_bd_cells xfft_0]

    make_bd_intf_pins_external  [get_bd_intf_pins xfft_0/S_AXIS_DATA]
    set_property name s_data [get_bd_intf_ports S_AXIS_DATA_0]
    make_bd_intf_pins_external  [get_bd_intf_pins xfft_0/S_AXIS_CONFIG]
    set_property name s_config [get_bd_intf_ports S_AXIS_CONFIG_0]

    make_bd_intf_pins_external  [get_bd_intf_pins xfft_0/M_AXIS_DATA]
    set_property name m_data [get_bd_intf_ports M_AXIS_DATA_0]
    make_bd_pins_external  [get_bd_pins xfft_0/event_frame_started]
    set_property name event_frame_started [get_bd_ports event_frame_started_0]
    make_bd_pins_external  [get_bd_pins xfft_0/event_data_in_channel_halt] [get_bd_pins xfft_0/event_status_channel_halt] [get_bd_pins xfft_0/event_tlast_missing] [get_bd_pins xfft_0/event_tlast_unexpected] [get_bd_pins xfft_0/event_data_out_channel_halt]
    set_property name event_tlast_unexpected [get_bd_ports event_tlast_unexpected_0]
    set_property name event_tlast_missing [get_bd_ports event_tlast_missing_0]
    set_property name event_status_channel_halt [get_bd_ports event_status_channel_halt_0]
    set_property name event_data_in_channel_halt [get_bd_ports event_data_in_channel_halt_0]
    set_property name event_data_out_channel_halt [get_bd_ports event_data_out_channel_halt_0]
    make_bd_pins_external  [get_bd_pins xfft_0/aclk]
    set_property name clkin [get_bd_ports aclk_0]


    validate_bd_design
    regenerate_bd_layout
    save_bd_design


    make_wrapper -files [get_files ./fft/fft_sim/FFT_test2/FFT_test2.srcs/sources_1/bd/design_1/design_1.bd] -top
    add_files -norecurse ./fft/fft_sim/FFT_test2/FFT_test2.srcs/sources_1/bd/design_1/hdl/design_1_wrapper.v




    
* Testbench 'tb_fft.v' and run the simulation 
   
.. code-block:: verilog

        `timescale 1 ns / 1 ps

        module tb_fft;
            reg clkin;
            wire event_data_in_channel_halt;
            wire event_data_out_channel_halt;
            wire event_frame_started;
            wire event_status_channel_halt;
            wire event_tlast_missing;
            wire event_tlast_unexpected;
            wire [31:0]m_data_tdata;
            wire m_data_tlast;
            reg m_data_tready;
            wire m_data_tvalid;
            reg [15:0]s_config_tdata;
            wire s_config_tready;
            reg s_config_tvalid;
            reg [31:0]s_data_tdata;
            reg s_data_tlast;
            wire s_data_tready;
            reg s_data_tvalid;
            
            reg signed [15:0]re_in;
            reg signed [15:0]im_in;
            reg signed [15:0]re_out;
            reg signed [15:0]im_out;                 ///shouldn't they be wire?
              

            design_1_wrapper wraptest(
                .clkin (clkin),
                .event_data_in_channel_halt (event_data_in_channel_halt),
                .event_data_out_channel_halt(event_data_out_channel_halt),
                .event_frame_started(event_frame_started),
                .event_status_channel_halt(event_status_channel_halt),
                .event_tlast_missing(event_tlast_missing),
                .event_tlast_unexpected(event_tlast_unexpected),
                .m_data_tdata(m_data_tdata),
                .m_data_tlast(m_data_tlast),
                .m_data_tready(m_data_tready),
                .m_data_tvalid(m_data_tvalid),
                .s_config_tdata(s_config_tdata),
                .s_config_tready(s_config_tready),
                .s_config_tvalid(s_config_tvalid),
                .s_data_tdata(s_data_tdata),
                .s_data_tlast(s_data_tlast),
                .s_data_tready(s_data_tready),
                .s_data_tvalid(s_data_tvalid));
          
          
          
        integer file;
        integer readerr = 1;
        integer errorno;

        reg[63:0] str;
          
          
        initial begin
            clkin = 0;
            re_in = 0;
            im_in = 0;
            re_out = 0;
            im_out = 0;
            m_data_tready = 0;
            s_config_tdata = 0;
            s_config_tvalid = 0;
            s_data_tdata = 0;
            s_data_tlast = 0;
            s_data_tvalid = 0;
            m_data_tready = 1;
            
           #5
            if(s_config_tready) begin
                s_config_tvalid = 1;
                s_config_tdata = 16'b1;
            end
            else begin 
                s_config_tvalid = 0;
            end
            file = $fopen("/proj/css/meherp/tb/test_fft.txt", "r");
            errorno = $ferror(file,str);
            if(!file) begin
                $display("file# %d\n", file);
                $finish;
            end

            while(!$feof(file)) begin
                if(s_data_tready) begin
                    readerr = $fscanf(file, "%d %d", re_in, im_in);

                    s_data_tvalid = 1;
                    s_data_tlast = 1;
                    @(posedge clkin);
                    s_data_tdata = {im_in, re_in};           

                end
            end
            s_data_tvalid = 0;
        end

        always @(m_data_tdata) begin
            re_out = m_data_tdata[15:0];
            im_out = m_data_tdata[31:16];
            $display("\nreal output %b\nimag output %d\n",re_out, im_out);
        end
        always
        #2.5 clkin = !clkin;

        endmodule


