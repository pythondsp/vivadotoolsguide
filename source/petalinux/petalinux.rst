Petalinux
*********

Hello World
===========

Vivado
------

.. code-block:: tcl
    :linenos:
    :caption: Create project

    # hello_petalinux.tcl
    # Vivado 2018.3
    # Board ZCU104


    create_project hello_petalinux /proj/css/meherp/practice/v2018_3/petalinux/zcu104/hello_petalinux -part xczu7ev-ffvc1156-2-e
    set_property board_part xilinx.com:zcu104:part0:1.1 [current_project]
    create_bd_design "design_1"
    update_compile_order -fileset sources_1

    # Zynq MPSoC
    create_bd_cell -type ip -vlnv xilinx.com:ip:zynq_ultra_ps_e:3.2 zynq_ultra_ps_e_0
    apply_bd_automation -rule xilinx.com:bd_rule:zynq_ultra_ps_e -config {apply_board_preset "1" }  [get_bd_cells zynq_ultra_ps_e_0]
    set_property -dict [list CONFIG.PSU__USE__M_AXI_GP0 {0} CONFIG.PSU__USE__M_AXI_GP1 {0} CONFIG.PSU__USE__FABRIC__RST {0}] [get_bd_cells zynq_ultra_ps_e_0]

    validate_bd_design
    save_bd_design

    make_wrapper -files [get_files /proj/css/meherp/practice/v2018_3/petalinux/zcu104/hello_petalinux/hello_petalinux.srcs/sources_1/bd/design_1/design_1.bd] -top
    add_files -norecurse /proj/css/meherp/practice/v2018_3/petalinux/zcu104/hello_petalinux/hello_petalinux.srcs/sources_1/bd/design_1/hdl/design_1_wrapper.v

    launch_runs impl_1 -to_step write_bitstream -jobs 8


* Below is the generated project, 

.. _`fig_petalinux2`:

.. figure:: petalinux/petalinux2.png
   :width: 100%

   Generate project 


SDK
---

Next create a SDK project with following settings, 

.. _`fig_petalinux1`:

.. figure:: petalinux/petalinux1.png
   :width: 100%

   Petalinux project settings


.. code-block:: cpp
    :linenos:
    :caption: Hello World

    #include <stdio.h>

    int main()
    {
        printf("Hello World\n");

        return 0;
    }


Petalinux
---------


.. code-block:: shell

    # source petalinux
    source /proj/petalinux/2018.3/petalinux-v2018.3_daily_latest/tool/petalinux-v2018.3-final/settings.sh

    # create project : choose correct location of .bsp
    petalinux-create --type project --source ../../../../../petalinux_ex/bsp/xilinx-zcu104-v2018.3-final-v2.bsp --name hello_petalinux

    cd hello_petalinux/

    # choose correct location of .sdk file
    petalinux-config --get-hw-description=../../hello_petalinux.sdk/

    petalinux-build
    petalinux-package --boot --format BIN --fsbl images/linux/zynqmp_fsbl.elf  --fpga ../../hello_petalinux.runs/impl_1/design_1_wrapper.bit --u-boot --force



SD card
-------


Copy below files in SD-card, 

* <petalinux-project>/images/linux/BOOT.INI (copy to root location)
* <petalinux-project>/images/linux/image.ub (copy to root location)
* <vivado-project>/hello_petalinux/hello_petalinux.sdk/hello_petalinux/Debug/hello_petalinux.elf
  

Insert the SD card in FPGA board and turn on the FPGA, 

.. code-block:: shell

    xilinx-zcu104-2018_3 login: root
    Password: root
    root@xilinx-zcu104-2018_3:~# mount /dev/mmcblk0p1 /mnt
    root@xilinx-zcu104-2018_3:~# cd /mnt/

    root@xilinx-zcu104-2018_3:/mnt# ls
    BOOT.BIN                   hello_petalinux.elf
    System Volume Information  image.ub

    root@xilinx-zcu104-2018_3:/mnt# ./hello_petalinux.elf
    Hello World



Half adder
==========



Vivado
------


.. code-block:: tcl
    :linenos:
    :caption: Create half adder project

    # half_adder_zynq.rst
    # Board ZCU104
    # Vivado 2018.3

    create_project half_adder_zynq ./half_adder_zynq -part xczu7ev-ffvc1156-2-e
    set_property board_part xilinx.com:zcu104:part0:1.1 [current_project]
    set_property target_language VHDL [current_project]


    # new block design "half_adder"
    create_bd_design "half_adder"

    # add vector logic
    create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 util_vector_logic_0
    # set vector logic to "xor"
    set_property -dict [list CONFIG.C_OPERATION {xor} CONFIG.LOGO_FILE {data/sym_xorgate.png}] [get_bd_cells util_vector_logic_0]
    # rename as "xor1"
    set_property name xor1 [get_bd_cells util_vector_logic_0]

    # copy above block "xor1"
    copy_bd_objs /  [get_bd_cells {xor1}]
    # xor to and gate
    set_property -dict [list CONFIG.C_OPERATION {and} CONFIG.LOGO_FILE {data/sym_andgate.png}] [get_bd_cells xor2]
    # rename "and1"
    set_property name and1 [get_bd_cells xor2]

    # input/output size = 1 bit
    set_property -dict [list CONFIG.C_SIZE {1}] [get_bd_cells xor1]
    set_property -dict [list CONFIG.C_SIZE {1}] [get_bd_cells and1]

    # make connections
    make_bd_pins_external  [get_bd_cells xor1]
    make_bd_intf_pins_external  [get_bd_cells xor1]
    set_property name a [get_bd_ports Op1_0]
    set_property name b [get_bd_ports Op2_0]
    set_property name sum [get_bd_ports Res_0]
    connect_bd_net [get_bd_ports a] [get_bd_pins and1/Op1]
    connect_bd_net [get_bd_ports b] [get_bd_pins and1/Op2]
    make_bd_pins_external  [get_bd_pins and1/Res]
    set_property name carry [get_bd_ports Res_0]


    # create hierarchy for Half adder
    group_bd_cells half_adder [get_bd_cells xor1] [get_bd_cells and1]

    delete_bd_objs [get_bd_nets Op2_0_1] [get_bd_ports b]
    delete_bd_objs [get_bd_nets Op1_0_1] [get_bd_ports a]
    delete_bd_objs [get_bd_nets xor1_Res] [get_bd_ports sum]
    delete_bd_objs [get_bd_nets and1_Res] [get_bd_ports carry]


    # Add zynq
    create_bd_cell -type ip -vlnv xilinx.com:ip:zynq_ultra_ps_e:3.2 zynq_ultra_ps_e_0
    apply_bd_automation -rule xilinx.com:bd_rule:zynq_ultra_ps_e -config {apply_board_preset "1" }  [get_bd_cells zynq_ultra_ps_e_0]
    set_property -dict [list CONFIG.PSU__USE__M_AXI_GP1 {0}] [get_bd_cells zynq_ultra_ps_e_0]


    # Axi interconnect
    create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 axi_interconnect_0

    # Gpio
    create_bd_cell -type ip -vlnv xilinx.com:ip:axi_gpio:2.0 axi_gpio_0
    set_property name gpio_x_y [get_bd_cells axi_gpio_0]
    set_property -dict [list CONFIG.C_GPIO_WIDTH {2} CONFIG.C_DOUT_DEFAULT {0x00000011} CONFIG.C_ALL_OUTPUTS {1}] [get_bd_cells gpio_x_y]

    create_bd_cell -type ip -vlnv xilinx.com:ip:axi_gpio:2.0 axi_gpio_0
    set_property name gpio_sum_carry [get_bd_cells axi_gpio_0]
    set_property -dict [list CONFIG.C_ALL_INPUTS {1}] [get_bd_cells gpio_sum_carry]
    set_property -dict [list CONFIG.C_GPIO_WIDTH {2}] [get_bd_cells gpio_sum_carry]


    # add slice for splitting the input
    create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_0
    set_property -dict [list CONFIG.DIN_TO {1} CONFIG.DIN_FROM {1} CONFIG.DIN_WIDTH {2} CONFIG.DIN_FROM {1} CONFIG.DOUT_WIDTH {1}] [get_bd_cells xlslice_0]
    create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_1
    set_property -dict [list CONFIG.DIN_WIDTH {2} CONFIG.DIN_TO {0} CONFIG.DIN_FROM {0}] [get_bd_cells xlslice_1]

    # concat
    create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 xlconcat_0

    # connections 
    connect_bd_intf_net [get_bd_intf_pins zynq_ultra_ps_e_0/M_AXI_HPM0_FPD] -boundary_type upper [get_bd_intf_pins axi_interconnect_0/S00_AXI]
    connect_bd_intf_net -boundary_type upper [get_bd_intf_pins axi_interconnect_0/M00_AXI] [get_bd_intf_pins gpio_x_y/S_AXI]
    connect_bd_intf_net -boundary_type upper [get_bd_intf_pins axi_interconnect_0/M01_AXI] [get_bd_intf_pins gpio_sum_carry/S_AXI]
    connect_bd_net [get_bd_pins gpio_x_y/gpio_io_o] [get_bd_pins xlslice_0/Din]
    connect_bd_net [get_bd_pins xlslice_1/Din] [get_bd_pins gpio_x_y/gpio_io_o]
    connect_bd_net [get_bd_pins xlslice_0/Dout] [get_bd_pins half_adder/a]
    connect_bd_net [get_bd_pins xlslice_1/Dout] [get_bd_pins half_adder/b]
    connect_bd_net [get_bd_pins half_adder/sum] [get_bd_pins xlconcat_0/In0]
    connect_bd_net [get_bd_pins half_adder/carry] [get_bd_pins xlconcat_0/In1]
    connect_bd_net [get_bd_pins xlconcat_0/dout] [get_bd_pins gpio_sum_carry/gpio_io_i]
    apply_bd_automation -rule xilinx.com:bd_rule:clkrst -config {Clk "/zynq_ultra_ps_e_0/pl_clk0 (100 MHz)" }  [get_bd_pins zynq_ultra_ps_e_0/maxihpm0_fpd_aclk]
    apply_bd_automation -rule xilinx.com:bd_rule:clkrst -config {Clk "/zynq_ultra_ps_e_0/pl_clk0 (100 MHz)" }  [get_bd_pins axi_interconnect_0/ACLK]
    apply_bd_automation -rule xilinx.com:bd_rule:clkrst -config {Clk "/zynq_ultra_ps_e_0/pl_clk0 (100 MHz)" }  [get_bd_pins axi_interconnect_0/M00_ACLK]
    apply_bd_automation -rule xilinx.com:bd_rule:clkrst -config {Clk "/zynq_ultra_ps_e_0/pl_clk0 (100 MHz)" }  [get_bd_pins axi_interconnect_0/M01_ACLK]


    assign_bd_address
    validate_bd_design
    save_bd_design


    make_wrapper -files [get_files ./half_adder_zynq/half_adder_zynq.srcs/sources_1/bd/half_adder/half_adder.bd] -top
    add_files -norecurse ./half_adder_zynq/half_adder_zynq.srcs/sources_1/bd/half_adder/hdl/half_adder_wrapper.vhd

    launch_runs impl_1 -to_step write_bitstream -jobs 40


.. SDK
.. ---

.. Next create a SDK project with following settings, 

.. .. _`fig_petalinux1`:

.. .. figure:: petalinux/petalinux1.png
..    :width: 100%

..    Petalinux project settings


.. .. code-block:: cpp
..     :linenos:
..     :caption: Hello World

..     #include <stdio.h>

..     int main()
..     {
..         printf("Hello World\n");

..         return 0;
..     }




.. Petalinux
.. ---------


.. .. code-block:: shell

..     # source petalinux 
..     source /proj/petalinux/2018.3/petalinux-v2018.3_daily_latest/tool/petalinux-v2018.3-final/settings.sh

..     # create project : choose correct location of .bsp
..     petalinux-create --type project --source ../../petalinux_ex/bsp/xilinx-zcu104-v2018.3-final-v2.bsp --name half_adder_petalinux
    
..     cd half_adder_petalinux

..     # choose correct location of .sdk file
..     # configuration: Use default settings (save and exit)
..     petalinux-config --get-hw-description=../half_adder_zynq/half_adder_zynq.sdk/
..     petalinux-build

..     # create FSBL and BOOT image
..     petalinux-package --boot --format BIN --fsbl images/linux/zynqmp_fsbl.elf  --fpga ../half_adder_zynq/half_adder_zynq.runs/impl_1/half_adder_wrapper.bit --u-boot --force



.. SD card
.. -------


.. Copy below files in SD-card, 

.. * <petalinux-project>/images/linux/BOOT.INI (copy to root location)
.. * <petalinux-project>/images/linux/image.ub (copy to root location)
.. * <vivado-project>/half_adder_zynq/half_adder_zynq.sdk/half_adder_linux_a53/Debug/half_adder_linux_a53.elf
  

.. Insert the SD card in FPGA board and turn on the FPGA, 

.. .. code-block:: shell

..     xilinx-zcu104-2018_3 login: root
..     Password: root
..     root@xilinx-zcu104-2018_3:~# mount /dev/mmcblk0p1 /mnt
..     root@xilinx-zcu104-2018_3:~# cd /mnt/

..     root@xilinx-zcu104-2018_3:/mnt# ls
..     BOOT.BIN                   half_adder_linux_a53.elf
..     System Volume Information  image.ub

..     root@xilinx-zcu104-2018_3:/mnt# ./half_adder_linux_a53.elf
..     Hello World
