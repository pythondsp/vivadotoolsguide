SDK
***


Half Adder
==========

* In this section, a TCL script is written to generate the Half adder, and then it is connected with Zynq processor. 

* Next a 'c' code is written to send the input to Half-adder via Zynq and the output is printed on the console. 

TCL Script
----------

Below is the 'tcl' script to create a Half adder design with Zynq connection, 

.. code-block:: tcl
    :linenos:
    :caption: TCL script to connect Half adder with Zynq 

    # Half adder 
    # board ZC706
    # Vivado 2018.3

    # create project
    create_project -force full_adder ./full_adder -part xc7z045ffg900-2
    set_property board_part xilinx.com:zc706:part0:1.4 [current_project]
    set_property target_language VHDL [current_project]

    # new block design "half_adder"
    create_bd_design "half_adder"

    # add vector logic 
    create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 util_vector_logic_0
    # set vector logic to "xor"
    set_property -dict [list CONFIG.C_OPERATION {xor} CONFIG.LOGO_FILE {data/sym_xorgate.png}] [get_bd_cells util_vector_logic_0]
    # rename as "xor1"
    set_property name xor1 [get_bd_cells util_vector_logic_0]

    # copy above block "xor1"
    copy_bd_objs /  [get_bd_cells {xor1}]
    # xor to and gate
    set_property -dict [list CONFIG.C_OPERATION {and} CONFIG.LOGO_FILE {data/sym_andgate.png}] [get_bd_cells xor2]
    # rename "and1"
    set_property name and1 [get_bd_cells xor2]

    # input/output size = 1 bit
    set_property -dict [list CONFIG.C_SIZE {1}] [get_bd_cells xor1]
    set_property -dict [list CONFIG.C_SIZE {1}] [get_bd_cells and1]

    # make connections 
    make_bd_pins_external  [get_bd_cells xor1]
    make_bd_intf_pins_external  [get_bd_cells xor1]
    set_property name a [get_bd_ports Op1_0]
    set_property name b [get_bd_ports Op2_0]
    set_property name sum [get_bd_ports Res_0]
    connect_bd_net [get_bd_ports a] [get_bd_pins and1/Op1]
    connect_bd_net [get_bd_ports b] [get_bd_pins and1/Op2]
    make_bd_pins_external  [get_bd_pins and1/Res]
    set_property name carry [get_bd_ports Res_0]



    # create hierarchy for Half adder
    group_bd_cells half_adder [get_bd_cells xor1] [get_bd_cells and1]

    # Add zynq
    create_bd_cell -type ip -vlnv xilinx.com:ip:processing_system7:5.5 processing_system7_0
    apply_bd_automation -rule xilinx.com:bd_rule:processing_system7 -config {make_external "FIXED_IO, DDR" apply_board_preset "1" Master "Disable" Slave "Disable" }  [get_bd_cells processing_system7_0]
    set_property -dict [list CONFIG.PCW_QSPI_GRP_IO1_ENABLE {1} CONFIG.PCW_TTC0_PERIPHERAL_ENABLE {0}] [get_bd_cells processing_system7_0]

    # Axi interconnect
    create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 axi_interconnect_0

    # Gpio
    create_bd_cell -type ip -vlnv xilinx.com:ip:axi_gpio:2.0 axi_gpio_0
    set_property name gpio_x_y [get_bd_cells axi_gpio_0]
    set_property -dict [list CONFIG.C_GPIO_WIDTH {2} CONFIG.C_DOUT_DEFAULT {0x00000011} CONFIG.C_ALL_OUTPUTS {1}] [get_bd_cells gpio_x_y]

    create_bd_cell -type ip -vlnv xilinx.com:ip:axi_gpio:2.0 axi_gpio_0
    set_property name gpio_sum_carry [get_bd_cells axi_gpio_0]
    set_property -dict [list CONFIG.C_ALL_INPUTS {1}] [get_bd_cells gpio_sum_carry]
    set_property -dict [list CONFIG.C_GPIO_WIDTH {2}] [get_bd_cells gpio_sum_carry]

    # connections
    connect_bd_intf_net [get_bd_intf_pins processing_system7_0/M_AXI_GP0] -boundary_type upper [get_bd_intf_pins axi_interconnect_0/S00_AXI]
    connect_bd_intf_net -boundary_type upper [get_bd_intf_pins axi_interconnect_0/M00_AXI] [get_bd_intf_pins gpio_x_y/S_AXI]
    connect_bd_intf_net -boundary_type upper [get_bd_intf_pins axi_interconnect_0/M01_AXI] [get_bd_intf_pins gpio_sum_carry/S_AXI]


    # add slice for splitting the input
    create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_0
    set_property -dict [list CONFIG.DIN_TO {1} CONFIG.DIN_FROM {1} CONFIG.DIN_WIDTH {2} CONFIG.DIN_FROM {1} CONFIG.DOUT_WIDTH {1}] [get_bd_cells xlslice_0]
    create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_1
    set_property -dict [list CONFIG.DIN_WIDTH {2} CONFIG.DIN_TO {0} CONFIG.DIN_FROM {0}] [get_bd_cells xlslice_1]

    connect_bd_net [get_bd_pins gpio_x_y/gpio_io_o] [get_bd_pins xlslice_1/Din]
    connect_bd_net [get_bd_pins xlslice_0/Din] [get_bd_pins gpio_x_y/gpio_io_o]
    delete_bd_objs [get_bd_nets Op1_0_1] [get_bd_ports a]
    delete_bd_objs [get_bd_nets Op2_0_1] [get_bd_ports b]
    connect_bd_net [get_bd_pins xlslice_0/Dout] [get_bd_pins half_adder/a]
    connect_bd_net [get_bd_pins xlslice_1/Dout] [get_bd_pins half_adder/b]
    delete_bd_objs [get_bd_nets and1_Res] [get_bd_nets xor1_Res] [get_bd_ports sum] [get_bd_ports carry]

    # concat
    create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 xlconcat_0

    connect_bd_net [get_bd_pins half_adder/sum] [get_bd_pins xlconcat_0/In0]
    connect_bd_net [get_bd_pins half_adder/carry] [get_bd_pins xlconcat_0/In1]
    connect_bd_net [get_bd_pins xlconcat_0/dout] [get_bd_pins gpio_sum_carry/gpio_io_i]

    # auto connection
    apply_bd_automation -rule xilinx.com:bd_rule:clkrst -config {Clk "/processing_system7_0/FCLK_CLK0 (50 MHz)" }  [get_bd_pins processing_system7_0/M_AXI_GP0_ACLK]
    apply_bd_automation -rule xilinx.com:bd_rule:clkrst -config {Clk "/processing_system7_0/FCLK_CLK0 (50 MHz)" }  [get_bd_pins axi_interconnect_0/ACLK]
    apply_bd_automation -rule xilinx.com:bd_rule:clkrst -config {Clk "/processing_system7_0/FCLK_CLK0 (50 MHz)" }  [get_bd_pins axi_interconnect_0/M00_ACLK]
    apply_bd_automation -rule xilinx.com:bd_rule:clkrst -config {Clk "/processing_system7_0/FCLK_CLK0 (50 MHz)" }  [get_bd_pins axi_interconnect_0/M01_ACLK]

    # assign base address
    assign_bd_address

    regenerate_bd_layout
    validate_bd_design -force
    save_bd_design


    make_wrapper -files [get_files ./full_adder/full_adder.srcs/sources_1/bd/half_adder/half_adder.bd] -top
    add_files -norecurse ./full_adder/full_adder.srcs/sources_1/bd/half_adder/hdl/half_adder_wrapper.vhd
    launch_runs impl_1 -to_step write_bitstream -jobs 40

    # export hardware
    # launch SDK
    # create Hello world project
    # replace code with 'c' code



* Below is the block diagram which is generated by above script, 


.. _`fig_sdk1`:

.. figure:: sdk/sdk1.png
   :width: 100%

   Half adder connection with Zynq


Generate bit file
-----------------

Do following, 

* Generate bitstream 
* Export Hardware (along with bitstream)
* Launch SDK


SDK
---

* Create the 'hello world' project
* Next, replace the 'hello world' code with below code, 

.. code-block:: cpp
    :linenos:
    :caption: Input and output for Half adder

    #include "xparameters.h"
    #include "xgpio.h"
    #include <stdio.h>
    #include "xstatus.h"
    #include "xil_printf.h"


    /************************** Variable Definitions **************************/

    XGpio GPIO_X_Y; /* The driver instance for GPIO Device configured as O/P */
    XGpio GPIO_SUM_CARRY;  /* The driver instance for GPIO Device configured as I/P */

    int main(void){
          u32 Result;

        // initialize GPIOs
          XGpio_Initialize(&GPIO_X_Y, XPAR_GPIO_X_Y_DEVICE_ID);
          XGpio_Initialize(&GPIO_SUM_CARRY, XPAR_GPIO_SUM_CARRY_DEVICE_ID);

          // output direction
          XGpio_SetDataDirection(&GPIO_X_Y, 1, 0xFFFFFFFF);        // 2 bit output from zynq : channel 1
          XGpio_SetDataDirection(&GPIO_SUM_CARRY, 1, 0x0); // 2 bit input to Zynq

          printf("\n############################################################################\n");
          printf("Outputs are displayed in integer format e.g. carry=1, sum=0 => 10 => 2\n");
          printf("############################################################################\n");

          XGpio_DiscreteWrite(&GPIO_X_Y, 1, 0x0);  // # give a specific value
          Result = XGpio_DiscreteRead(&GPIO_SUM_CARRY, 1);
          printf("00 = %d (carry:0, sum:0 => 00 => 0)\n", Result);

          XGpio_DiscreteWrite(&GPIO_X_Y, 1, 0x1);  // # give a specific value
          Result = XGpio_DiscreteRead(&GPIO_SUM_CARRY, 1);
          printf("01 = %d (carry:0, sum:1 => 01 => 1)\n", Result);

          XGpio_DiscreteWrite(&GPIO_X_Y, 1, 0x2);  // # give a specific value
          Result = XGpio_DiscreteRead(&GPIO_SUM_CARRY, 1);
          printf("10 = %d (carry:0, sum:1 => 01 => 1)\n", Result);

          XGpio_DiscreteWrite(&GPIO_X_Y, 1, 0x3);  // # give a specific value
          Result = XGpio_DiscreteRead(&GPIO_SUM_CARRY, 1);
          printf("11 = %d (carry:1, sum:0 => 10 => 2)\n", Result);

          return 0;
    }



* Below is the output of the code, 
  
.. code-block:: text
    :linenos:
    :caption: Output of the C code for Half adder

    ############################################################################
    Outputs are displayed in integer format e.g. carry=1, sum=0 => 10 => 2
    ############################################################################
    00 = 0 (carry:0, sum:0 => 00 => 0)
    01 = 1 (carry:0, sum:1 => 01 => 1)
    10 = 1 (carry:0, sum:1 => 01 => 1)
    11 = 2 (carry:1, sum:0 => 10 => 2)




BRAM
====


In this section, an interface between Zynq and BRAM is created. Then, the table of 5 is stored in BRAM through one BRAM controller and then stored values are read back using other BRAM controller. 


TCL script
----------


.. code-block:: tcl
    :linenos:
    :caption: TCL script to connect BRAM with Zynq 

    # bram_ex.tcl
    # board: ZC706
    # Vivado 2018.3

    create_project -force bram_zynq ./bram_sdk/bram_zynq -part xc7z045ffg900-2
    set_property board_part xilinx.com:zc706:part0:1.4 [current_project]

    create_bd_design "bram_zynq"

    # Zynq
    create_bd_cell -type ip -vlnv xilinx.com:ip:processing_system7:5.5 processing_system7_0
    apply_bd_automation -rule xilinx.com:bd_rule:processing_system7 -config {make_external "FIXED_IO, DDR" apply_board_preset "1" Master "Disable" Slave "Disable" }  [get_bd_cells processing_system7_0]
    set_property -dict [list CONFIG.PCW_QSPI_GRP_IO1_ENABLE {1} CONFIG.PCW_TTC0_PERIPHERAL_ENABLE {0}] [get_bd_cells processing_system7_0]

    # axi interconnect
    create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 axi_interconnect_0
    connect_bd_intf_net [get_bd_intf_pins processing_system7_0/M_AXI_GP0] -boundary_type upper [get_bd_intf_pins axi_interconnect_0/S00_AXI]
    apply_bd_automation -rule xilinx.com:bd_rule:clkrst -config {Clk "/processing_system7_0/FCLK_CLK0 (50 MHz)" }  [get_bd_pins processing_system7_0/M_AXI_GP0_ACLK]
    apply_bd_automation -rule xilinx.com:bd_rule:clkrst -config {Clk "/processing_system7_0/FCLK_CLK0 (50 MHz)" }  [get_bd_pins axi_interconnect_0/ACLK]


    # BRAM controller
    create_bd_cell -type ip -vlnv xilinx.com:ip:axi_bram_ctrl:4.1 axi_bram_ctrl_0
    set_property -dict [list CONFIG.SINGLE_PORT_BRAM {1}] [get_bd_cells axi_bram_ctrl_0]
    create_bd_cell -type ip -vlnv xilinx.com:ip:axi_bram_ctrl:4.1 axi_bram_ctrl_1
    set_property -dict [list CONFIG.SINGLE_PORT_BRAM {1}] [get_bd_cells axi_bram_ctrl_1]

    # Block memory generator
    create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.4 blk_mem_gen_0
    set_property -dict [list CONFIG.Memory_Type {True_Dual_Port_RAM} CONFIG.Enable_B {Use_ENB_Pin} CONFIG.Use_RSTB_Pin {true} CONFIG.Port_B_Clock {100} CONFIG.Port_B_Write_Rate {50} CONFIG.Port_B_Enable_Rate {100} CONFIG.EN_SAFETY_CKT {false}] [get_bd_cells blk_mem_gen_0]

    connect_bd_intf_net -boundary_type upper [get_bd_intf_pins axi_interconnect_0/M00_AXI] [get_bd_intf_pins axi_bram_ctrl_0/S_AXI]
    connect_bd_intf_net -boundary_type upper [get_bd_intf_pins axi_interconnect_0/M01_AXI] [get_bd_intf_pins axi_bram_ctrl_1/S_AXI]
    connect_bd_intf_net [get_bd_intf_pins axi_bram_ctrl_0/BRAM_PORTA] [get_bd_intf_pins blk_mem_gen_0/BRAM_PORTA]
    connect_bd_intf_net [get_bd_intf_pins axi_bram_ctrl_1/BRAM_PORTA] [get_bd_intf_pins blk_mem_gen_0/BRAM_PORTB]

    apply_bd_automation -rule xilinx.com:bd_rule:clkrst -config {Clk "/processing_system7_0/FCLK_CLK0 (50 MHz)" }  [get_bd_pins axi_interconnect_0/M00_ACLK]
    apply_bd_automation -rule xilinx.com:bd_rule:clkrst -config {Clk "/processing_system7_0/FCLK_CLK0 (50 MHz)" }  [get_bd_pins axi_interconnect_0/M01_ACLK]

    save_bd_design

    # assign baseaddress 
    exclude_bd_addr_seg [get_bd_addr_segs axi_bram_ctrl_1/S_AXI/Mem0] -target_address_space [get_bd_addr_spaces processing_system7_0/Data]
    exclude_bd_addr_seg [get_bd_addr_segs axi_bram_ctrl_0/S_AXI/Mem0] -target_address_space [get_bd_addr_spaces processing_system7_0/Data]
    delete_bd_objs [get_bd_addr_segs -excluded processing_system7_0/Data/SEG_axi_bram_ctrl_0_Mem0]
    delete_bd_objs [get_bd_addr_segs -excluded processing_system7_0/Data/SEG_axi_bram_ctrl_1_Mem0]
    assign_bd_address [get_bd_addr_segs {axi_bram_ctrl_0/S_AXI/Mem0 }]
    assign_bd_address [get_bd_addr_segs {axi_bram_ctrl_1/S_AXI/Mem0 }]
    set_property offset 0x50000000 [get_bd_addr_segs {processing_system7_0/Data/SEG_axi_bram_ctrl_1_Mem0}]
    set_property offset 0x40001000 [get_bd_addr_segs {processing_system7_0/Data/SEG_axi_bram_ctrl_0_Mem0}]


    regenerate_bd_layout
    validate_bd_design
    save_bd_design

    make_wrapper -files [get_files ./bram_sdk/bram_zynq/bram_zynq.srcs/sources_1/bd/bram_zynq/bram_zynq.bd] -top
    add_files -norecurse ./bram_sdk/bram_zynq/bram_zynq.srcs/sources_1/bd/bram_zynq/hdl/bram_zynq_wrapper.v


    launch_runs impl_1 -to_step write_bitstream -jobs 40


    # export hardware with bitstream
    # launch SDK
    # create hello world project


* :numref:`fig_sdk2` is the generated block design. 
  

.. _`fig_sdk2`:

.. figure:: sdk/sdk2.png
   :width: 100%

   BRAM connection with Zynq



SDK
---


.. code-block:: cpp
    :linenos:
    :caption: C code to store and read the table of 5


    #include <stdio.h>
    #include "platform.h"
    #include "xil_printf.h"
    #include "xparameters.h"
    #include "xil_io.h"

    int main()
    {
        init_platform();

        print("BRAM Test\n\r");

        for (int i=0; i<10; i++){
            // increase the address by 4 after each write
            Xil_Out32(XPAR_AXI_BRAM_CTRL_0_S_AXI_BASEADDR + 4*i, 5 * i); // store table of 5
        }

        for (int i=0; i<10; i++){
            // increase the address by 4 after each write
            printf("Address: %x, Value: %d\n", XPAR_AXI_BRAM_CTRL_1_S_AXI_BASEADDR + 4*i, Xil_In32(XPAR_AXI_BRAM_CTRL_1_S_AXI_BASEADDR + 4*i)); // display table of 5
        }


        cleanup_platform();
        return 0;
    }




* Below is the output of the code, 
  
.. code-block:: text
    :linenos:
    :caption: Output of the C code

    BRAM Test
    Address: 50000000, Value: 0
    Address: 50000004, Value: 5
    Address: 50000008, Value: 10
    Address: 5000000c, Value: 15
    Address: 50000010, Value: 20
    Address: 50000014, Value: 25
    Address: 50000018, Value: 30
    Address: 5000001c, Value: 35
    Address: 50000020, Value: 40
    Address: 50000024, Value: 45



FIFO
====

In this section, table of 5 is stored and printed from the FIFO.


TCL Script
----------


.. code-block:: tcl
    :linenos:
    :caption: Zynq FIFO connection 

    # fifo_zynq.tcl
    # vivado 2018.3
    # Board ZC706

    create_project -force fifo_axi ./fifo_test/fifo_axi -part xc7z045ffg900-2
    set_property board_part xilinx.com:zc706:part0:1.4 [current_project]
    create_bd_design "fifo_zqnq"

    # add zynq
    create_bd_cell -type ip -vlnv xilinx.com:ip:processing_system7:5.5 processing_system7_0
    apply_bd_automation -rule xilinx.com:bd_rule:processing_system7 -config {make_external "FIXED_IO, DDR" apply_board_preset "1" Master "Disable" Slave "Disable" }  [get_bd_cells processing_system7_0]
    set_property -dict [list CONFIG.PCW_USE_S_AXI_HP0 {1} CONFIG.PCW_QSPI_GRP_IO1_ENABLE {1} CONFIG.PCW_TTC0_PERIPHERAL_ENABLE {0} CONFIG.PCW_USB0_PERIPHERAL_ENABLE {0}] [get_bd_cells processing_system7_0]

    # AXI stream data fifo
    create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_data_fifo_0

    # AXI DMA
    create_bd_cell -type ip -vlnv xilinx.com:ip:axi_dma:7.1 axi_dma_0
    set_property -dict [list CONFIG.c_include_sg {0} CONFIG.c_sg_include_stscntrl_strm {0}] [get_bd_cells axi_dma_0]

    # AXI interconnect
    create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 axi_interconnect_0
    set_property -dict [list CONFIG.NUM_MI {1}] [get_bd_cells axi_interconnect_0]
    set_property -dict [list CONFIG.NUM_SI {2} CONFIG.NUM_MI {1}] [get_bd_cells axi_interconnect_0]

    # connection
    connect_bd_intf_net [get_bd_intf_pins axi_dma_0/M_AXIS_MM2S] [get_bd_intf_pins axis_data_fifo_0/S_AXIS]
    connect_bd_intf_net [get_bd_intf_pins axi_dma_0/M_AXI_MM2S] -boundary_type upper [get_bd_intf_pins axi_interconnect_0/S00_AXI]
    connect_bd_intf_net [get_bd_intf_pins axi_dma_0/M_AXI_S2MM] -boundary_type upper [get_bd_intf_pins axi_interconnect_0/S01_AXI]
    connect_bd_intf_net -boundary_type upper [get_bd_intf_pins axi_interconnect_0/M00_AXI] [get_bd_intf_pins processing_system7_0/S_AXI_HP0]
    apply_bd_automation -rule xilinx.com:bd_rule:clkrst -config {Clk "/processing_system7_0/FCLK_CLK0 (50 MHz)" }  [get_bd_pins processing_system7_0/S_AXI_HP0_ACLK]
    apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { Clk_master {Auto} Clk_slave {Auto} Clk_xbar {Auto} Master {/processing_system7_0/M_AXI_GP0} Slave {/axi_dma_0/S_AXI_LITE} intc_ip {New AXI Interconnect} master_apm {0}}  [get_bd_intf_pins axi_dma_0/S_AXI_LITE]
    apply_bd_automation -rule xilinx.com:bd_rule:clkrst -config {Clk "/processing_system7_0/FCLK_CLK0 (50 MHz)" }  [get_bd_pins axi_dma_0/m_axi_mm2s_aclk]
    apply_bd_automation -rule xilinx.com:bd_rule:clkrst -config {Clk "/processing_system7_0/FCLK_CLK0 (50 MHz)" }  [get_bd_pins axi_interconnect_0/ACLK]
    apply_bd_automation -rule xilinx.com:bd_rule:clkrst -config {Clk "/processing_system7_0/FCLK_CLK0 (50 MHz)" }  [get_bd_pins axi_interconnect_0/S01_ACLK]
    connect_bd_intf_net [get_bd_intf_pins axis_data_fifo_0/M_AXIS] [get_bd_intf_pins axi_dma_0/S_AXIS_S2MM]


    assign_bd_address
    regenerate_bd_layout
    validate_bd_design
    save_bd_design


    # create wrapper
    make_wrapper -files [get_files ./fifo_test/fifo_axi/fifo_axi.srcs/sources_1/bd/fifo_zqnq/fifo_zqnq.bd] -top
    add_files -norecurse ./fifo_test/fifo_axi/fifo_axi.srcs/sources_1/bd/fifo_zqnq/hdl/fifo_zqnq_wrapper.v

    # implementation
    launch_runs impl_1 -to_step write_bitstream -jobs 40

    # export hardware
    # open SDK
    # create Hello world project


* :numref:`fig_sdk3` is the block diagram generated by the above script, 

.. _`fig_sdk3`:

.. figure:: sdk/sdk3.png
   :width: 100%


SDK
---

* Below is the 'c' code the save and read the table of 5 from FIFO. 
  

.. code-block:: cpp
    :linenos:
    :caption: C code to send and read the table of 5 from FIFO

    
    /***************************** Include Files *********************************/
    #include <stdio.h>
    #include "xaxidma.h"
    #include "xparameters.h"
    #include "xil_printf.h"


    /******************** Constant Definitions **********************************/

    // Device hardware build related constants.
    #define DMA_DEV_ID      XPAR_AXIDMA_0_DEVICE_ID

    #define MEM_BASE_ADDR       0x01000000
    #define TX_BUFFER_BASE      (MEM_BASE_ADDR + 0x00100000)
    #define RX_BUFFER_BASE      (MEM_BASE_ADDR + 0x00300000)
    #define RX_BUFFER_HIGH      (MEM_BASE_ADDR + 0x004FFFFF)

    #define NUM_TABLE    0x5  // number to print the table
    #define NUM_ITER     0xa // print table upto NUM_ITER



    // Device instance definitions
    XAxiDma AxiDma;


    int main()
    {
        int Status;

        xil_printf("\r\n--- Entering main() --- \r\n");

        XAxiDma_Config *CfgPtr;
        int Index;
        u8 *TxBufferPtr;
        u8 *RxBufferPtr;
        u8 Value;

        TxBufferPtr = (u8 *)TX_BUFFER_BASE ;
        RxBufferPtr = (u8 *)RX_BUFFER_BASE;

        /* Initialize the XAxiDma device.
         */
        CfgPtr = XAxiDma_LookupConfig(DMA_DEV_ID);
        if (!CfgPtr) {
            xil_printf("No config found for %d\r\n", DMA_DEV_ID);
            return XST_FAILURE;
        }

        Status = XAxiDma_CfgInitialize(&AxiDma, CfgPtr);
        if (Status != XST_SUCCESS) {
            xil_printf("Initialization failed %d\r\n", Status);
            return XST_FAILURE;
        }

        if(XAxiDma_HasSg(&AxiDma)){
            xil_printf("Device configured as SG mode \r\n");
            return XST_FAILURE;
        }

        /* Disable interrupts, we use polling mode
         */
        XAxiDma_IntrDisable(&AxiDma, XAXIDMA_IRQ_ALL_MASK,
                            XAXIDMA_DEVICE_TO_DMA);
        XAxiDma_IntrDisable(&AxiDma, XAXIDMA_IRQ_ALL_MASK,
                            XAXIDMA_DMA_TO_DEVICE);

        // flush the RX buffer
        Xil_DCacheFlushRange((UINTPTR)RxBufferPtr, NUM_ITER);

        printf("################# Tx data ######################\n");
        for(Index = 0; Index < NUM_ITER; Index ++) {
                Value = NUM_TABLE*Index;
                TxBufferPtr[Index] = Value;
                printf("Value[%d]: %d\n", Index, Value);

        }

        Status = XAxiDma_SimpleTransfer(&AxiDma,(UINTPTR) RxBufferPtr,
                            NUM_ITER, XAXIDMA_DEVICE_TO_DMA);

        if (Status != XST_SUCCESS) {
            return XST_FAILURE;
        }

        Status = XAxiDma_SimpleTransfer(&AxiDma,(UINTPTR) TxBufferPtr,
                    NUM_ITER, XAXIDMA_DMA_TO_DEVICE);

        if (Status != XST_SUCCESS) {
            return XST_FAILURE;
        }

        // flush the TX buffer
        Xil_DCacheFlushRange((UINTPTR)TxBufferPtr, NUM_ITER);



        while ((XAxiDma_Busy(&AxiDma,XAXIDMA_DEVICE_TO_DMA)) ||
                   (XAxiDma_Busy(&AxiDma,XAXIDMA_DMA_TO_DEVICE))) {
               }

        printf("\n################# Rx data ######################\n");
        for(Index = 0; Index < NUM_ITER; Index ++) {
                printf("Value[%d]: %d\n", Index, RxBufferPtr[Index]);
        }

        return XST_SUCCESS;

    }





* Below is the output of the above code, 

.. code-block:: text
    :linenos: 

    ################# Tx data ######################
    Value[0]: 0
    Value[1]: 5
    Value[2]: 10
    Value[3]: 15
    Value[4]: 20
    Value[5]: 25
    Value[6]: 30
    Value[7]: 35
    Value[8]: 40
    Value[9]: 45

    ################# Rx data ######################
    Value[0]: 0
    Value[1]: 5
    Value[2]: 10
    Value[3]: 15
    Value[4]: 20
    Value[5]: 25
    Value[6]: 30
    Value[7]: 35
    Value[8]: 40
    Value[9]: 45


Switch and LED
==============


In this section, the value of switch-pattern (pushbutton) are read and then the patterns are displayed on LED e.g. if SW-pattern=1 then two LED are glow etc. 


TCL Script
----------


.. code-block:: tcl
    :linenos:
    :caption: Switch, LED and Zynq connection, 

    # fifo_zynq.tcl
    # vivado 2018.3
    # Board ZC706

    create_project -force sw_led_zynq ./sw_led_zynq -part xc7z045ffg900-2
    create_bd_design "sw_led"
    set_property board_part xilinx.com:zc706:part0:1.4 [current_project]
    reset_property board_connections [get_projects sw_led_zynq]
    set_property board_part xilinx.com:zc706:part0:1.4 [current_project]
    reset_property board_connections [get_projects sw_led_zynq]

    # Zynq
    create_bd_cell -type ip -vlnv xilinx.com:ip:processing_system7:5.5 processing_system7_0
    apply_bd_automation -rule xilinx.com:bd_rule:processing_system7 -config {make_external "FIXED_IO, DDR" apply_board_preset "1" Master "Disable" Slave "Disable" }  [get_bd_cells processing_system7_0]
    set_property -dict [list CONFIG.PCW_QSPI_GRP_IO1_ENABLE {1} CONFIG.PCW_TTC0_PERIPHERAL_ENABLE {0} CONFIG.PCW_USB0_PERIPHERAL_ENABLE {0}] [get_bd_cells processing_system7_0]

    # Axi interconnect 
    create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 axi_interconnect_0
    connect_bd_intf_net [get_bd_intf_pins processing_system7_0/M_AXI_GP0] -boundary_type upper [get_bd_intf_pins axi_interconnect_0/S00_AXI]

    create_bd_cell -type ip -vlnv xilinx.com:ip:axi_gpio:2.0 axi_gpio_0

    # GPIO with TWO ports i.e. SW and LED. LED is set to 5 (i.e. 101)
    set_property -dict [list CONFIG.NUM_MI {1}] [get_bd_cells axi_interconnect_0]
    set_property -dict [list CONFIG.C_GPIO_WIDTH {3} CONFIG.C_GPIO2_WIDTH {4} CONFIG.C_IS_DUAL {1} CONFIG.C_ALL_INPUTS {1} CONFIG.C_DOUT_DEFAULT_2 {0x00000005} CONFIG.GPIO_BOARD_INTERFACE {gpio_sws_3bits} CONFIG.GPIO2_BOARD_INTERFACE {led_4bits} CONFIG.C_ALL_OUTPUTS_2 {1}] [get_bd_cells axi_gpio_0]
    connect_bd_intf_net -boundary_type upper [get_bd_intf_pins axi_interconnect_0/M00_AXI] [get_bd_intf_pins axi_gpio_0/S_AXI]
    make_bd_intf_pins_external  [get_bd_intf_pins axi_gpio_0/GPIO] [get_bd_intf_pins axi_gpio_0/GPIO2]
    set_property name SW3 [get_bd_intf_ports GPIO_0]
    set_property name LED4 [get_bd_intf_ports GPIO2_0]

    # block automation
    apply_bd_automation -rule xilinx.com:bd_rule:clkrst -config {Clk "/processing_system7_0/FCLK_CLK0 (50 MHz)" }  [get_bd_pins processing_system7_0/M_AXI_GP0_ACLK]
    apply_bd_automation -rule xilinx.com:bd_rule:clkrst -config {Clk "/processing_system7_0/FCLK_CLK0 (50 MHz)" }  [get_bd_pins axi_interconnect_0/ACLK]
    apply_bd_automation -rule xilinx.com:bd_rule:clkrst -config {Clk "/processing_system7_0/FCLK_CLK0 (50 MHz)" }  [get_bd_pins axi_interconnect_0/M00_ACLK]


    assign_bd_address
    regenerate_bd_layout
    save_bd_design


    make_wrapper -files [get_files /proj/css/meherp/practice/v2018_3/sw_led_zynq/sw_led_zynq.srcs/sources_1/bd/sw_led/sw_led.bd] -top
    add_files -norecurse /proj/css/meherp/practice/v2018_3/sw_led_zynq/sw_led_zynq.srcs/sources_1/bd/sw_led/hdl/sw_led_wrapper.v

    launch_runs impl_1 -to_step write_bitstream -jobs 40



* Below is the design which is generated by above code, 

.. _`fig_sdk4`:

.. figure:: sdk/sdk4.png
   :width: 100%

   Zynq, LED and SW connection, 



SDK
---


Below is the C code which read the switch value and write a pattern on LED, 

.. code-block:: cpp
    :linenos:
    :caption: Read switch value and write patter on LED

    #include "xparameters.h"
    #include "xgpio.h"
    #include <stdio.h>
    #include "xstatus.h"
    #include "xil_printf.h"
    #include "platform.h"



    // GPIO : 1st channel : 3-SW
    //      : 2nd channel : 4-LED

    /************************** Variable Definitions **************************/

    XGpio SW_LED; /* both SW and LED are connected to same GPIO */


    int main()
    {
        init_platform();
        u32 InputData;

        // initialize GPIOs
        XGpio_Initialize(&SW_LED, XPAR_AXI_GPIO_0_DEVICE_ID);

        /* Set the direction for all signals to be input */
        // i.e. read values from the switch
         XGpio_SetDataDirection(&SW_LED, 1, 0xFFFFFFFF); // SW

         /* Set the direction for all signals to be output */
         // i.e. send output to LED
         XGpio_SetDataDirection(&SW_LED, 2, 0x0);  // LED

         while(1){
             // read switch
             InputData = XGpio_DiscreteRead(&SW_LED, 1);
             printf("SW: %d\n", (int)InputData); // print value

             // glow different LED based on switch pattern
             if ((int)InputData == 1) // if 1
                 XGpio_DiscreteWrite(&SW_LED, 2, 0x03); // 3: i.e. glow two LED "11"
             else if ((int)InputData == 2)
                 XGpio_DiscreteWrite(&SW_LED, 2, 0x07);
             else if ((int)InputData == 4)
                 XGpio_DiscreteWrite(&SW_LED, 2, 0x0f);
             else if ((int)InputData == 5)
                 XGpio_DiscreteWrite(&SW_LED, 2, 0x05);
             else
                 XGpio_DiscreteWrite(&SW_LED, 2, 0x0a);
         }

         cleanup_platform();
         return 0;
    }



* Please see the output on board by pressing different combination of SW (pushbutton). 



Speed calculation (HLS)
=======================

In this section, the equation "v = u + a \* t" is implemented in HLS. Then, it is used with Zynq in Vivado. 


HLS
---


.. code-block:: cpp
    :linenos:
    :caption: HLS code for "v = u + a * t"

    // speed_calc.cpp

    void speed_calc(float u, float t, float a, float *v)
    {
    #pragma HLS INTERFACE s_axilite port=return bundle=HLS_MACC_PERIPH_BUS
    #pragma HLS INTERFACE s_axilite port=u bundle=HLS_MACC_PERIPH_BUS
    #pragma HLS INTERFACE s_axilite port=a bundle=HLS_MACC_PERIPH_BUS
    #pragma HLS INTERFACE s_axilite port=t bundle=HLS_MACC_PERIPH_BUS
    #pragma HLS INTERFACE s_axilite port=v bundle=HLS_MACC_PERIPH_BUS

       *v = u + a * t;
    }


* Testbench 

.. code-block:: cpp
    :linenos:
    :caption: Testbench 

    // speed_calc_tb.cpp

    #include <stdio.h>

    void speed_calc(float u, float t, float a, float *v);

    int main(){
        float v_hw, v_sw, u=2, a=3, t=4;

        for (int i=0; i<10; i++){
            v_sw = u + a * t;
            speed_calc(u, t, a,  &v_hw);

            if (v_sw != v_hw){
                printf("Incorrect results");
                return 1;
            }
        }
        printf("Correct results");
        return 0;

    }



Vivado design
-------------

.. note:: 

    Please modify the Line 12 with correct location of HLS solution. In below line, the 'speed_calc' is the folder which contatins the solution; Vivado automatically detects all the IPs which are inside the parent folder. 

    set_property  ip_repo_paths  /practice/v2018_3/Project_area/speed_calc [current_project]



.. code-block:: tcl
    :linenos:
    :emphasize-lines: 12 
    :caption: TCL script to generate the design 

    # speed_calc.tcl
    # board : ZC706
    # Vivado : 2018.3

    # modify the line "set_property ip_repo_paths /<location of HLS solution>"

    create_project -force speed_calc_sdk ./speed_calc_sdk -part xc7z045ffg900-2
    set_property board_part xilinx.com:zc706:part0:1.4 [current_project]
    create_bd_design "design_1"
    update_compile_order -fileset sources_1
    # modify below line with correct location of HLS solution
    set_property ip_repo_paths  /<location of HLS solution> [current_project]
    update_ip_catalog -rebuild -scan_changes


    # Zynq
    create_bd_cell -type ip -vlnv xilinx.com:ip:processing_system7:5.5 processing_system7_0
    apply_bd_automation -rule xilinx.com:bd_rule:processing_system7 -config {make_external "FIXED_IO, DDR" apply_board_preset "1" Master "Disable" Slave "Disable" }  [get_bd_cells processing_system7_0]
    set_property -dict [list CONFIG.PCW_QSPI_GRP_IO1_ENABLE {1} CONFIG.PCW_TTC0_PERIPHERAL_ENABLE {0}] [get_bd_cells processing_system7_0]

    # HLS design
    create_bd_cell -type ip -vlnv xilinx.com:hls:speed_calc:1.0 speed_calc_0

    # Axi interconnect
    create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 axi_interconnect_0
    set_property -dict [list CONFIG.NUM_MI {1}] [get_bd_cells axi_interconnect_0]

    # connection
    connect_bd_intf_net [get_bd_intf_pins processing_system7_0/M_AXI_GP0] -boundary_type upper [get_bd_intf_pins axi_interconnect_0/S00_AXI]
    connect_bd_intf_net -boundary_type upper [get_bd_intf_pins axi_interconnect_0/M00_AXI] [get_bd_intf_pins speed_calc_0/s_axi_HLS_MACC_PERIPH_BUS]
    apply_bd_automation -rule xilinx.com:bd_rule:clkrst -config {Clk "/processing_system7_0/FCLK_CLK0 (50 MHz)" }  [get_bd_pins processing_system7_0/M_AXI_GP0_ACLK]
    apply_bd_automation -rule xilinx.com:bd_rule:clkrst -config {Clk "/processing_system7_0/FCLK_CLK0 (50 MHz)" }  [get_bd_pins speed_calc_0/ap_clk]
    apply_bd_automation -rule xilinx.com:bd_rule:clkrst -config {Clk "/processing_system7_0/FCLK_CLK0 (50 MHz)" }  [get_bd_pins axi_interconnect_0/ACLK]


    assign_bd_address
    validate_bd_design
    regenerate_bd_layout
    save_bd_design


    make_wrapper -files [get_files ./speed_calc_sdk/speed_calc_sdk.srcs/sources_1/bd/design_1/design_1.bd] -top
    add_files -norecurse ./speed_calc_sdk/speed_calc_sdk.srcs/sources_1/bd/design_1/hdl/design_1_wrapper.v

    launch_runs impl_1 -to_step write_bitstream -jobs 40


    # export the project
    # launch SDK
    # create Hello world project


* :numref:`fig_sdk5` is the generated design, 

.. _`fig_sdk5`:

.. figure:: sdk/sdk5.png
   :width: 100%

   HLS IP integration with Zynq



SDK
---

Below is the SDK code, which sends the data to FPGA and read back the results. 


.. code-block:: cpp
    :linenos:
    :caption: SDK code 

    #include <stdio.h>
    #include <math.h>
    #include "platform.h"
    #include "xil_printf.h"
    #include "xspeed_calc.h"


    // ###########################################################
    // ### Conversion required to send data to Zynq via SDK

    // u32 to float conversion
    float u32_to_float(unsigned int val){
        union {
            float val_float;
            unsigned char bytes[4];
        } data;

        data.bytes[3] = (val >> (8*3)) & 0xff;
        data.bytes[2] = (val >> (8*2)) & 0xff;
        data.bytes[1] = (val >> (8*1)) & 0xff;
        data.bytes[0] = (val >> (8*0)) & 0xff;

        return data.val_float;
    }


    // float to u32 conversion
    unsigned int float_to_u32(float val){
        unsigned int result;
        union float_bytes{
            float v;
            unsigned char bytes[4];
        } data;

        data.v = val;
        result = (data.bytes[3] << 24) + (data.bytes[2] << 16) + (data.bytes[1] << 8) + (data.bytes[0]);

        return result;
    }

    // ###########################################################


    int main()
    {
        init_platform();

        print("Connected to board\n\r");

        float u=10.5, a=2;
        int status;
        XSpeed_calc doCalc;
        XSpeed_calc_Config *doCalc_config;

        doCalc_config = XSpeed_calc_LookupConfig(XPAR_SPEED_CALC_0_DEVICE_ID);
        if (!doCalc_config){
            printf("HLS IP configuration failed\n");
            return 1;
        }
        else{
            printf("IP is configured successfully\n");
        }


        status = XSpeed_calc_CfgInitialize(&doCalc, doCalc_config);
        if (status != XST_SUCCESS){
            printf("HLS IP initialization failed\n");
            return 1;
        }
        else{
            printf("IP is initialized successfully\n");
        }

        for (int t=0; t<10; t++){
            XSpeed_calc_Set_u(&doCalc, float_to_u32(u));
            XSpeed_calc_Set_a(&doCalc, float_to_u32(a));
            XSpeed_calc_Set_t(&doCalc, float_to_u32(t));
            XSpeed_calc_Start(&doCalc);


            while(!XSpeed_calc_IsDone(&doCalc));
            float speed_hw = 0, speed_sw = 0;

            speed_sw = u + a * t;
            speed_hw = u32_to_float(XSpeed_calc_Get_v(&doCalc));

            float errCalc = fabsf(speed_hw - speed_sw);
            printf("Test[%d]: SW=[%f], HW=[%f], error=[%f]\n", t, speed_sw, speed_hw, errCalc);
        }

        cleanup_platform();
        return 0;
    }




Results
-------

.. code-block:: text
    :linenos:
    :caption: Software (SDK) and Hardware (HLS IP) Results
 
    Connected to board
    IP is configured successfully
    IP is initialized successfully
    Test[0]: SW=[10.500000], HW=[10.500000], error=[0.000000]
    Test[1]: SW=[12.500000], HW=[12.500000], error=[0.000000]
    Test[2]: SW=[14.500000], HW=[14.500000], error=[0.000000]
    Test[3]: SW=[16.500000], HW=[16.500000], error=[0.000000]
    Test[4]: SW=[18.500000], HW=[18.500000], error=[0.000000]
    Test[5]: SW=[20.500000], HW=[20.500000], error=[0.000000]
    Test[6]: SW=[22.500000], HW=[22.500000], error=[0.000000]
    Test[7]: SW=[24.500000], HW=[24.500000], error=[0.000000]
    Test[8]: SW=[26.500000], HW=[26.500000], error=[0.000000]
    Test[9]: SW=[28.500000], HW=[28.500000], error=[0.000000]

