// gateEx.h

// go to folder
// >> cd C:\adder_cpp
// >> xmcImportFunction('meher_lib',{'and1'},'gateEx.h',{},{})

#include <stdio.h>

void and1(bool x, bool y, bool *z);

void and1(bool x, bool y, bool *z){  
    *z = x & y;
}

void nand1(bool x, bool y, bool *z){  
    *z = !(x & y);
}

void or1(bool x, bool y, bool *z){  
    *z = x | y;
}

void nor1(bool x, bool y, bool *z){  
    *z = !(x | y);
}

void xor1(bool x, bool y, bool *z){  
    *z = x ^ y;
}

void xnor1(bool x, bool y, bool *z){  
    *z = !(x ^ y);
}