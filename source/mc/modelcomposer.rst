Model composer
**************



Import HLS design to Model composer
===================================

Save the following function in a folder e.g. hls. 


.. code-block:: c
    :linenos:
    :caption: Half adder

    // gateEx.h

    // go to folder
    // >> cd C:\hls
    // >> xmcImportFunction('meher_lib',{'and1'},'gateEx.h',{},{})

    #include <stdio.h>

    void and1(bool x, bool y, bool *z);

    void and1(bool x, bool y, bool *z){  
        *z = x & y;
    }

    void nand1(bool x, bool y, bool *z){  
        *z = !(x & y);
    }

    void or1(bool x, bool y, bool *z){  
        *z = x | y;
    }

    void nor1(bool x, bool y, bool *z){  
        *z = !(x | y);
    }

    void xor1(bool x, bool y, bool *z){  
        *z = x ^ y;
    }

    void xnor1(bool x, bool y, bool *z){  
        *z = !(x ^ y);
    }


* Open Matlab and go to this folder using command 'cd'. And import the above function to Model composer using below command. 

.. code-block:: matlab

    >> xmcImportFunction('meher_lib',{'and1', 'nand1', 'or1', 'nor1', 'xor1', 'xnor1'},'gateEx.h',{},{})


.. note:: 

    * xmcImportFunction function has following format, 
    
    .. code-block:: matlab
    
        >> xmcImportFunction('adder_mc',{'adder'}, 'adder_ex.h',{adder1.cpp, mult2.cpp},{'$XILINX_VIVADO_HLS/include'})

    * We can import a specific function as well. 

    .. code-block:: matlab
    
        >> xmcImportFunction('meher_lib',{'and1'},'gateEx.h',{},{})

    * Use override and unlock operations as below, 
    
    .. code-block:: matlab
    
        xmcImportFunction('meher_lib',{'and1'},'gateEx.h',{},{},'unlock', 'override')


The above command will import all the functions to file 'meher_lib' as shown in :numref:`mc_fig_1`.
Save it in the same folder where the hls-code was saved.


.. _`mc_fig_1`:

.. figure:: img/1.png
   :width: 30%

   Imported HLS block by xmcImportFunction command



Next open a new .slx file use these blocks and run it to see the results as shown in :numref:`mc_fig_2`. The values of x and y are [0,1,0,1] and [0,0,1,1] respectively

.. _`mc_fig_2`:

.. figure:: img/2.png
   :width: 100%

   Simulation results of HLS blocks x=[0,1,0,1], y=[0,0,1,1]




Mathematical operations
=======================

Four Mathematical operations are performed in below code, 

.. code-block:: c

    // math_meher_lib.cpp
    
    #include <stdio.h>

    void adder1(const double a, const double b, double *result) {
            *result = a + b;
        }


    void subtract1(const double a, const double b, double *result) {
            *result = a - b;
        }

    void multiply1(const double a, const double b, double *result) {
            *result = a * b;
        }

    void divide1(const double a, const double b, double *result) {
            *result = a / b;
        }


* Add the these operations to 'meher_lib', 

.. code-block:: matlab

    >> xmcImportFunction('meher_lib',{'adder1', 'subtract1', 'multiply1', 'divide1'},'math_meher_lib.h',{'math_meher_lib.cpp'},{}, 'override')


The above command will import all the functions to file 'meher_lib' as shown in :numref:`mc_fig_3`.


.. _`mc_fig_3`:

.. figure:: img/3.png
   :width: 30%

   Imported HLS block by xmcImportFunction command



Next open a new .slx file use these blocks and run it to see the results as shown in :numref:`mc_fig_4`.

.. _`mc_fig_4`:

.. figure:: img/4.png
   :width: 50%

   Simulation results of HLS blocks


Accept different fixed point length
===================================

In the below code, an adder is implemented which can perform on different fixed point values (but both the inputs should have same fixed point values), 


.. code-block:: c

    // fixed_point_adder_ex.cpp
    // >> xmcImportFunction('meher_lib',{'fixed_add'}, 'fixed_point_adder_ex.cpp',{},{'$XILINX_VIVADO_HLS/include'})

    #include <stdint.h>
    #include <ap_fixed.h>

    template <int WordLen, int IntLen>

    void fixed_add(
        const ap_fixed<WordLen,IntLen> a,
        const ap_fixed<WordLen,IntLen> b,
        ap_fixed<WordLen+1,IntLen> &sum){
        
            sum = a + b;        
    }


* Import above code to Model composer, 

.. code-block:: matlab

    >> xmcImportFunction('meher_lib',{'fixed_add'}, 'fixed_point_adder_ex.cpp',{},{'$XILINX_VIVADO_HLS/include'})


The above command will import the function to file 'meher_lib' as shown in :numref:`mc_fig_5`.


.. _`mc_fig_5`:

.. figure:: img/5.png
   :width: 30%

   Imported HLS block by xmcImportFunction command



Next open a new .slx file use this blocks and run it to see the results as shown in :numref:`mc_fig_6`.

.. _`mc_fig_6`:

.. figure:: img/6.png
   :width: 50%

   Simulation results of HLS blocks (Fixed point is set to 16-12)


Multitype adder using XMC pragma
================================

In the below code, values of two matrix will be added. 
The input type can be int16, int32, double and single only, which is set by using "pragma XMC SUPPORTED_TYPES". The other types can not be added here. 

.. code-block:: c

    // multitype_adder.c
    // >> xmcImportFunction('meher_lib',{'matrix_add'}, 'multitype_adder.c',{},{'$XILINX_VIVADO_HLS/include'}, 'override')

    // Input must be a matrix
    // input type can be only int16, int32, double, single

    #include <stdint.h>
    #pragma XMC INPORT x // define x as inport
    #pragma XMC INPORT y // define y as inport
    #pragma XMC OUTPORT z // define z as outport
    #pragma XMC SUPPORTED_TYPES T: int16, int32, double, single

    template <class T, int ROWS, int COLS>
    void matrix_add(
        const T x[ROWS][COLS],
        const T y[ROWS][COLS],
        T z[ROWS][COLS]) 
        {
            for (int i = 0; i<ROWS; i++) {
                for (int j = 0; j<COLS; j++) {
                    z[i][j] = x[i][j] + y[i][j];
                }
            }
        }

            

* Use below code to import the HLS code to model composer, 

.. code-block:: matlab

    xmcImportFunction('meher_lib',{'matrix_add'}, 'multitype_adder.c',{},{'$XILINX_VIVADO_HLS/include'}, 'override')


The above command will import the function to file 'meher_lib' as shown in :numref:`mc_fig_7`.


.. _`mc_fig_7`:

.. figure:: img/7.png
   :width: 30%

   Imported HLS block by xmcImportFunction command



Next open a new .slx file use this blocks and run it to see the results as shown in :numref:`mc_fig_8`.

.. _`mc_fig_8`:

.. figure:: img/8.png
   :width: 50%

   Simulation results of HLS blocks (for int_16 input)


* If we change the input type to int_8, then error will be reported, 

.. _`mc_fig_9`:

.. figure:: img/9.png
   :width: 90%

   Error in Simulation (for int_8 input)


FIR Filter
==========

Below is the 12 tap FIR filter. 

.. code-block:: c

    // fir.h

    #define N 11

    #pragma XMC INPORT c

    void fir (
      double *y,
      double c[N+1],
      double x
      );


.. code-block:: c

    // fir.c
    // >> xmcImportFunction('meher_lib',{'fir'},'fir.h',{'fir.cpp'},{},'unlock','override')

    #include "fir.h"

    void fir (
      double *y,
      double c[N],
      double x
      ) {

      static double shift_reg[N];
      double acc;
      double data;
      int i;
      
      acc=0;
      Shift_Accum_Loop: for (i=N-1;i>=0;i--) {
            if (i==0) {
                shift_reg[0]=x;
            data = x;
        } else {
                shift_reg[i]=shift_reg[i-1];
                data = shift_reg[i];
        }
        acc+=data*c[i];;       
      }
      *y=acc;
    }

Run below command to import the FIR design to Model composer, 

.. code-block:: matlab

    >> xmcImportFunction('meher_lib',{'fir'},'fir.h',{'fir.cpp'},{},'unlock','override')


.. _`mc_fig_10`:

.. figure:: img/10.png
   :width: 20%

   Imported HLS block by xmcImportFunction command



Next open a new .slx file use this blocks and add blocks to input and output ports as shown in :numref:`mc_fig_11`. The value of port c is [0,0,0.25,0.5,0.75,1,-1,-0.75,-0.5,-0.25,0,0]. Note that, a reshape filter is added before input 'c' to change the 'array' to 'vector' in Model-composer version 2018.3. 

.. _`mc_fig_11`:

.. figure:: img/11.png
   :width: 80%

   Add input and outputs to FIR block 

Next, run the simulation to see the results as shown in :numref:`mc_fig_12`, 


.. _`mc_fig_12`:

.. figure:: img/12.png
   :width: 50%

   Simulation result for c=[0,0,0.25,0.5,0.75,1,-1,-0.75,-0.5,-0.25,0,0]

