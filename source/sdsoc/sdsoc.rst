SDSoC
*****



Half Adder
==========


.. code-block:: c
    :linenos:
    :caption: Half adder

    // half_adder.cpp

    # include <stdio.h>
    #include "ap_int.h"

    int half_adder(ap_int<1> x, ap_int<1> y, ap_int<1> *sum, ap_int<1> *carry){
        *sum = x ^ y; // * is inout port
        *carry = x & y;

        return 0;
    }


.. code-block:: c

    // top.cpp

    #include <stdio.h>
    #include "ap_int.h"

    int half_adder(ap_int<1>, ap_int<1>, ap_int<1>*, ap_int<1>*);

    int main(int argc, char* argv[]) {
        ap_int<1> sum, carry;
        printf("Running main.... \n");

        half_adder(1, 0, &sum, &carry);
        printf("sum %d, carry %d\n", sum.to_uint(), carry.to_uint());

        half_adder(1, 1, &sum, &carry);
        printf("sum %d, carry %d\n", sum.to_uint(), carry.to_uint());

        return 0;
    }


