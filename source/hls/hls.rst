HLS
***

Introduction
============

We can use HLS codes in Model composer to implement the bigger designs. Hence, in this chapter, some simple examples of HLS are shown. 


Basics
======

In this section, we will implement a full adder using half adders. 
Next, we will import this design to SysGen and Vivado IP integrator and check the simulation results as well. 

Half Adder
----------
    
Here, a half adder is implemented along with testbench. 

.. note:: 

    Boolean types are defined using 'bool' and 'ap_uint'. 
    Either use 'bool' or 'ap_uint' (don't mix them together to use below codes)


.. code-block:: c
    :linenos:
    :caption: Half adder

    # include <stdio.h>

    void half_adder(bool x, bool y, bool *sum, bool *carry){
        *sum = x ^ y; // * is inout port
        *carry = x & y;
    }


or 

.. code-block:: c
    :linenos:
    :caption: Half adder

    # include <stdio.h>
    #include "ap_int.h"

    int half_adder(ap_uint<1> x, ap_uint<1> y, ap_uint<1> *sum, ap_uint<1> *carry){
        *sum = x ^ y; // * is inout port
        *carry = x & y;

        return 0;
    }




* Testbench

.. code-block:: c
    :linenos:
    :caption: Half adder testbench

    # include <stdio.h>

    void half_adder(bool, bool, bool*, bool*);

    int main(){
        bool sum, carry;
        half_adder(1, 1, &sum, &carry);
        printf("sum %d, carry %d\n", sum, carry);

        return 0;
    }



or 

.. code-block:: c
    :linenos:
    :caption: Half adder testbench

    # include <stdio.h>
    #include "ap_int.h"

    int half_adder(ap_uint<1>, ap_uint<1>, ap_uint<1>*, ap_uint<1>*);

    int main(){
        ap_uint<1> sum, carry;
        half_adder(1, 0, &sum, &carry);
        printf("sum %d, carry %d\n", sum.to_uint(), carry.to_uint());

        half_adder(1, 1, &sum, &carry);
        printf("sum %d, carry %d\n", sum.to_uint(), carry.to_uint());

        return 0;
    }


Full adder using half adder
---------------------------

Here, half adders are used to create the full adder. 


.. code-block:: c
    :linenos:
    :caption: Full adder using half adder

    # include <stdio.h>
    #include <ap_cint.h>

    void half_adder(bool x, bool y, bool *sum, bool *carry){
        *sum = x ^ y; // * is inout port
        *carry = x & y;
    }

    void full_adder(bool a, bool b, bool c, bool *sum, bool *carry){
        bool hf1_sum, hf1_carry; // sum and carry for half adder 1
        bool hf2_sum, hf2_carry;

        half_adder(a, b, &hf1_sum, &hf1_carry);
        half_adder(c, hf1_sum, &hf2_sum, &hf2_carry);

        *sum = hf2_sum;
        *carry = hf1_carry || hf2_carry;

    }



or

.. code-block:: c
    :linenos:
    :caption: Full adder using half adder

    // full_adder.cpp

    # include <stdio.h>
    #include "ap_int.h"

    void half_adder(ap_uint<1> x, ap_uint<1> y, ap_uint<1> *sum, ap_uint<1> *carry){
        *sum = x ^ y; // * is inout port
        *carry = x & y;
    }

    void full_adder(ap_uint<1> a, ap_uint<1> b, ap_uint<1> c, ap_uint<1> *sum, ap_uint<1> *carry){
        ap_uint<1> hf1_sum, hf1_carry; // sum and carry for half adder 1
        ap_uint<1> hf2_sum, hf2_carry;

        half_adder(a, b, &hf1_sum, &hf1_carry);
        half_adder(c, hf1_sum, &hf2_sum, &hf2_carry);

        *sum = hf2_sum;
        *carry = hf1_carry || hf2_carry;

    }


* Testbench

.. code-block:: c
    :linenos:
    :caption: Full adder testbench

    # include <stdio.h>
    #include <ap_cint.h>

    void full_adder(bool, bool, bool, bool*, bool*);

    int main(){
        bool sum, carry;

        full_adder(1, 1, 1, &sum, &carry);
        printf("sum %d, carry %d\n", sum, carry);

        full_adder(1, 0, 1, &sum, &carry);
        printf("sum %d, carry %d\n", sum, carry);

        return 0;
    }

or


.. note:: 

    Below is the full adder testbench for all the input patterns. 

.. code-block:: c
    :linenos:
    :caption: Full adder testbench

    // full_adder_tb.cpp

    # include <stdio.h>
    #include "ap_int.h"

    void full_adder(ap_uint<1>, ap_uint<1>, ap_uint<1>, ap_uint<1>*, ap_uint<1>*);

    int main(){
        ap_uint<1> sum, carry;
        ap_uint<1> x[8] = {0, 1, 0, 1, 0, 1, 0, 1};
        ap_uint<1> y[8] = {0, 0, 1, 1, 0, 0, 1, 1};
        ap_uint<1> z[8] = {0, 0, 0, 0, 1, 1, 1, 1};

        for (int i=0; i<8; i++){
            full_adder(x[i], y[i], z[i], &sum, &carry);
            printf("x=%d, y=%d, z=%d, sum=%d, carry=%d\n",
                    x[i].to_uint(), y[i].to_uint(), z[i].to_uint(),
                    sum.to_uint(), carry.to_uint());

        }

        return 0;
    }


* Below is the output of above testbench, 

.. code-block:: text

    x=0, y=0, z=0, sum=0, carry=0
    x=1, y=0, z=0, sum=1, carry=0
    x=0, y=1, z=0, sum=1, carry=0
    x=1, y=1, z=0, sum=0, carry=1
    x=0, y=0, z=1, sum=1, carry=0
    x=1, y=0, z=1, sum=0, carry=1
    x=0, y=1, z=1, sum=0, carry=1
    x=1, y=1, z=1, sum=1, carry=1


Export as SysGen design
-----------------------

* Next, run synthesis, simulation and export RTL (sysgen) by clicking buttons in :numref:`fig_hls2` one by one. Select 'export RTL as IP' as shown in :numref:`fig_hls7`
 
.. _`fig_hls2`:

.. figure:: hls/hls2.png
   :width: 30%

   Synthesis, simulation and export RTL


.. _`fig_hls7`:

.. figure:: hls/hls7.png
   :width: 50%

   Export RTL as SysGen 



* Finally, import the sysgen design to System Generator using HLS block. Next complete the design as shown in :numref:`fig_hls1` and run the simulation. 

.. _`fig_hls1`:

.. figure:: hls/hls1.png
   :width: 75%

   Import sysgen design and make connection. 



Export as IP catalog
--------------------

* Next, run synthesis, simulation and export RTL (Vivado) by clicking buttons in :numref:`fig_hls2` one by one. Select 'export RTL as IP' as shown in :numref:`fig_hls6`


.. _`fig_hls6`:

.. figure:: hls/hls6.png
   :width: 50%

   Export RTL as IP  


* Next open the exported project which is saved inside the folder 'solution->impl->VHDL/Verilog'. 

* Modify the design as shown in :numref:`fig_hls4`


.. _`fig_hls4`:

.. figure:: hls/hls4.png
   :width: 75%

   Modify connections

* Select 'block design' as synthesis design as shown in :numref:`fig_hls5`

.. _`fig_hls5`:

.. figure:: hls/hls5.png
   :width: 50%

   Select model for simulation

* Finally run the simulation and see the results as shown in :numref:`fig_hls3`

.. _`fig_hls3`:

.. figure:: hls/hls3.png
   :width: 75%

   Simulation results


Edge detection
==============

In this section, edge detection is implemented usin 'hls_video' library and then 'open_cv' library is used to write the testbench. Sobel-Y fiter coefficients are used, which can be replaced with other coefficients as well which are shown below, 

C-simulation
------------

In this section, C-simulation is done. 

.. code-block:: text

    sobel y 
        [-1, -2, -1],
        [0, 0, 0],
        [1, 2, 1]

    emboss 
        [-2, -1, 0],
        [-1, 1, 1],
        [0, 1, 2]

    sobelX
        [-1, 0, 1],
        [-2, 0, 2],
        [-1, 0, 1]


Code for edge detection using sobel-y filter, 

.. code-block:: c

    // sobel_y.cpp

    #include <hls_video.h>
    #include <stdint.h>

    void sobel_y(uint8_t image_in[480*480],uint8_t image_out[480*480]){

        // coefficients for sobel y
       const char coefficients[3][3] = {
                                         {-1,-2,-1},
                                         { 0, 0, 0},
                                         { 1, 2, 1}
                                       };

       hls::Mat<480,480,HLS_8UC1> src;
       hls::Mat<480,480,HLS_8UC1> dst;
       hls::AXIM2Mat<480,uint8_t,480,480,HLS_8UC1>(image_in,src);
       hls::Window<3,3,char> kernel;

       for (int i=0;i<3;i++){
          for (int j=0;j<3;j++){
             kernel.val[i][j]=coefficients[i][j];
          }
       }
       hls::Point_<int> anchor = hls::Point_<int>(-1,-1);
       hls::Filter2D(src,dst,kernel,anchor);
       hls::Mat2AXIM<480,uint8_t,480,480,HLS_8UC1>(dst,image_out);
    }



Testbench

.. code-block:: c

    // tb_soble_y.cpp

    #include <hls_opencv.h>
    #include <stdint.h>
    #include <stdio.h>
    using namespace cv;

    void sobel_y(uint8_t * image_in, uint8_t * image_out);
    int main(){
       Mat im = imread("test.jpg",CV_LOAD_IMAGE_GRAYSCALE);
       uint8_t image_in[480*480];
       uint8_t image_out[480*480];

       memcpy(image_in,im.data,sizeof(uint8_t)*480*480);
       sobel_y(image_in,image_out);

       Mat out = Mat(480,480,CV_8UC1,image_out);

       namedWindow("sobel_y");
       imshow("sobel_y",out);
       waitKey(0);

     return 0;
    }


:numref:`fig_hls8` is the original 'test.jpg', 

.. _`fig_hls8`:

.. figure:: hls/hls8.png
   :width: 50%

   Original image 


Run the 'c simulation' and we we get the output as shown in :numref:`fig_hls9`, 

.. _`fig_hls9`:

.. figure:: hls/hls9.png
   :width: 50%

   Edge detect using Sobel-Y filter 



Synthesis
---------

Now, synthesize the above design and we will get below error, 

.. error:: 

    ERROR: [XFORM 203-733] An internal stream 'src.data_stream[0].V' (conv/sobel_y.cpp:10) with default size is used in a non-dataflow region, which may result in deadlock. Please consider to resize the stream using the directive 'set_directive_stream' or the 'HLS stream' pragma.

    ERROR: [XFORM 203-733] An internal stream 'dst.data_stream[0].V' (conv/sobel_y.cpp:11) with default size is used in a non-dataflow region, which may result in deadlock. Please consider to resize the stream using the directive 'set_directive_stream' or the 'HLS stream' pragma.

    ERROR: [HLS 200-70] Pre-synthesis failed.




To remove the error, add the pragma as below, 


.. code-block:: c

    // sobel_y.cpp

    #include <hls_video.h>
    #include <stdint.h>

    void sobel_y(uint8_t image_in[480*480],uint8_t image_out[480*480]){

    // depth =  480x480 = 230400
    #pragma HLS INTERFACE m_axi depth=230400 port=image_in
    #pragma HLS INTERFACE m_axi depth=230400 port=image_out

        // coefficients for sobel y
    #pragma HLS DATAFLOW
       const char coefficients[3][3] = {
                                         {-1,-2,-1},
                                         { 0, 0, 0},
                                         { 1, 2, 1}
                                       };

       hls::Mat<480,480,HLS_8UC1> src;
       hls::Mat<480,480,HLS_8UC1> dst;
       hls::AXIM2Mat<480,uint8_t,480,480,HLS_8UC1>(image_in,src);
       hls::Window<3,3,char> kernel;

       for (int i=0;i<3;i++){
          for (int j=0;j<3;j++){
             kernel.val[i][j]=coefficients[i][j];
          }
       }
       hls::Point_<int> anchor = hls::Point_<int>(-1,-1);
       hls::Filter2D(src,dst,kernel,anchor);
       hls::Mat2AXIM<480,uint8_t,480,480,HLS_8UC1>(dst,image_out);
    }
