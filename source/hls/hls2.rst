HLS Examples
************

In this chapter we will some of the HLS-example for better understanding of the pragmas. 


Definitions
===========




FIR
===


First FIR Design
----------------


.. code-block:: c
    :linenos:
    :caption: FIR filter (unoptimized due to 'if' statement) 

    // fir1.cpp

    // this is an unoptimized design due to 
    // 'if' statement inside the 'for' loop

    #define N 11
    #include "ap_int.h"

    // typedef is good to change the type of data in later time 
    typedef int coef_t;
    typedef int data_t;
    typedef int acc_t; 

    void fir1(data_t *y, data_t x){
        coef_t c[N] = {
            53, 0, -91, 0, 313, 500, 313, 0, -91, 0, 53
        };

        static data_t shift_reg[N];
        acc_t acc; 
        int i;

        acc = 0; 
        Shift_Accum_Loop:
            for (i=N-1; i>=0; i--){
                if (i==0){
                    acc += x*c[0];
                    shift_reg[0] = x; 
                }
                else{
                    shift_reg[i] = shift_reg[i-1]; 
                    acc += shift_reg[i]*c[i];
                }
            }

        *y = acc; 
    }

* Testbench: We will use this testbench for all the modified version of above FIR. 



.. code-block:: c
    :linenos:
    :caption: Testbench: Impulse response of FIR 

    // fir1_tb.cpp

    #include <stdio.h>

    // typedef is good to change the type of data in later time
    typedef int coef_t;
    typedef int data_t;
    typedef int acc_t;

    void fir1(data_t *y, data_t x);

    int main(){
        data_t x[11] = {0};
        data_t y;

        x[0] = 1; 

        for (int i=0; i<11; i++){
            fir1(&y, x[i]);
            printf("%d: y=%d\n",i, y);
        }
        
        return 0;
    }


* Output of the testbench, 

.. code-block:: text
    :linenos:
    :caption: Output of FIR filter for impulse 

    0: y=53
    1: y=0
    2: y=-91
    3: y=0
    4: y=313
    5: y=500
    6: y=313
    7: y=0
    8: y=-91
    9: y=0
    10: y=53

* Perform 'synthesis' to generate the estimate report, 

.. _`fig_hls10`:

.. figure:: hls/hls10.png
   :width: 50%

   Performance and resource estimate


Remove 'if' statement
---------------------

Here, 'if' statement is removed from the 'for' loop; and loop is run till 'i>0' (instead of 'i>=0'). 

.. code-block:: c
    :linenos:
    :caption: FIR filter (without 'if' statement) 

    // fir1.cpp

    #define N 11
    #include "ap_int.h"

    // typedef is good to change the type of data in later time 
    typedef int coef_t;
    typedef int data_t;
    typedef int acc_t; 

    void fir1(data_t *y, data_t x){
        coef_t c[N] = {
            53, 0, -91, 0, 313, 500, 313, 0, -91, 0, 53
        };

        static data_t shift_reg[N];
        acc_t acc; 
        int i;

        acc = 0; 
        Shift_Accum_Loop:
            for (i=N-1; i>0; i--){
                    shift_reg[i] = shift_reg[i-1]; 
                    acc += shift_reg[i]*c[i];
            }

        acc += x*c[0];
        shift_reg[0] = x; 

        *y = acc; 
    }

* Now we can see that less number of "Total resources" for this design. Also, the value of max latency is reduced. 
  

.. _`fig_hls11`:

.. figure:: hls/hls11.png
   :width: 50%

   Performance and resource estimate


Separate 'for' loop
-------------------

.. note::
    
    * Advantage of separating the loops: Better optimization can be performed on separate loops.
    
    * Without optimization, the separation 'for' loop may improve the performance, but reverse is also true. 


.. code-block:: c
    :linenos:
    :caption: FIR filter (after separating the 'for' loop) 

    // fir1.cpp

    #define N 11
    #include "ap_int.h"

    // typedef is good to change the type of data in later time
    typedef int coef_t;
    typedef int data_t;
    typedef int acc_t;

    void fir1(data_t *y, data_t x){
        coef_t c[N] = {
            53, 0, -91, 0, 313, 500, 313, 0, -91, 0, 53
        };

        static data_t shift_reg[N];
        acc_t acc;
        int i;

        Tapped_delay:
            for (i=N-1; i>0; i--){
                    shift_reg[i] = shift_reg[i-1];   
            }

        shift_reg[0] = x;
        acc = 0;
        MAC:
            for (i=N-1; i>=0; i--){
                acc += shift_reg[i]*c[i];
            }    

        *y = acc;
    }


.. _`fig_hls12`:

.. figure:: hls/hls12.png
   :width: 50%

   Performance and resource estimate


Loop unrolling
--------------

.. code-block:: c
    :linenos:
    :caption: Unroll = 2

    // fir1.cpp

    #define N 11
    #include "ap_int.h"

    // typedef is good to change the type of data in later time
    typedef int coef_t;
    typedef int data_t;
    typedef int acc_t;

    void fir1(data_t *y, data_t x){
        coef_t c[N] = {
            53, 0, -91, 0, 313, 500, 313, 0, -91, 0, 53
        };

        static data_t shift_reg[N];
        acc_t acc;
        int i;

        Tapped_delay:
            for (i=N-1; i>0; i--){
    #pragma HLS UNROLL factor=4
                    shift_reg[i] = shift_reg[i-1];
            }

        shift_reg[0] = x;
        acc = 0;
        MAC:
            for (i=N-1; i>=0; i--){
                acc += shift_reg[i]*c[i];
            }

        *y = acc;
    }
 




.. _`fig_hls13`:

.. figure:: hls/hls13.png
   :width: 50%

   Estimate for unrolling = 2


.. _`fig_hls14`:

.. figure:: hls/hls14.png
   :width: 50%

   Estimate for unrolling = 4

.. _`fig_hls15`:

.. figure:: hls/hls15.png
   :width: 50%

   Estimate for unrolling = auto


Array partition
---------------

.. code-block:: c
    :linenos:
    :caption: Array partition 

    // fir1.cpp

    #define N 11
    #include "ap_int.h"

    // typedef is good to change the type of data in later time
    typedef int coef_t;
    typedef int data_t;
    typedef int acc_t;

    void fir1(data_t *y, data_t x){
        coef_t c[N] = {
            53, 0, -91, 0, 313, 500, 313, 0, -91, 0, 53
        };

        static data_t shift_reg[N];
    #pragma HLS ARRAY_PARTITION variable=shift_reg complete dim=1
        acc_t acc;
        int i;

        Tapped_delay:
            for (i=N-1; i>0; i--){
    #pragma HLS UNROLL factor=4
                    shift_reg[i] = shift_reg[i-1];
            }

        shift_reg[0] = x;
        acc = 0;
        MAC:
            for (i=N-1; i>=0; i--){
                acc += shift_reg[i]*c[i];
            }

        *y = acc;
    }



.. _`fig_hls16`:

.. figure:: hls/hls16.png
   :width: 50%

   Estimate for unrolling = 4, ARRAY_PARTITION = complete


.. _`fig_hls17`:

.. figure:: hls/hls17.png
   :width: 50%

   Estimate ARRAY_PARTITION = complete (without unrolling)

Loop pipelining
---------------

.. code-block:: c
    :linenos:
    :caption: pipelining with II = 2

    // fir1.cpp

    #define N 11
    #include "ap_int.h"

    // typedef is good to change the type of data in later time
    typedef int coef_t;
    typedef int data_t;
    typedef int acc_t;

    void fir1(data_t *y, data_t x){
        coef_t c[N] = {
            53, 0, -91, 0, 313, 500, 313, 0, -91, 0, 53
        };

        static data_t shift_reg[N];
        acc_t acc;
        int i;

        Tapped_delay:
            for (i=N-1; i>0; i--){
    #pragma HLS PIPELINE II=2
                    shift_reg[i] = shift_reg[i-1];
            }

        shift_reg[0] = x;
        acc = 0;
        MAC:
            for (i=N-1; i>=0; i--){
                acc += shift_reg[i]*c[i];
            }

        *y = acc;
    }

.. _`fig_hls18`:

.. figure:: hls/hls18.png
   :width: 50%

   Pipeline with II = 2


Input/Output width
------------------


.. code-block:: cpp
    :linenos:
    :caption: Optimization using proper bit-width

    // fir1.cpp

    #define N 11
    #include "ap_int.h"

    // typedef is good to change the type of data in later time
    typedef ap_int<10> coef_t;
    typedef ap_int<10> data_t;
    typedef ap_int<10> acc_t;

    void fir1(data_t *y, data_t x){
        coef_t c[N] = {
            53, 0, -91, 0, 313, 500, 313, 0, -91, 0, 53
        };

        static data_t shift_reg[N];
        acc_t acc;
        int i;

        Tapped_delay:
            for (i=N-1; i>0; i--){
                    shift_reg[i] = shift_reg[i-1];
            }

        shift_reg[0] = x;
        acc = 0;
        MAC:
            for (i=N-1; i>=0; i--){
                acc += shift_reg[i]*c[i];
            }

        *y = acc;
    }


* Testbench 

.. code-block:: c
    :linenos:
    :caption: Testbench: Impulse response of FIR 
    
    // fir1_tb.cpp

    #include <stdio.h>
    #include "ap_int.h"

    // typedef is good to change the type of data in later time
    typedef ap_int<10> coef_t;
    typedef ap_int<10> data_t;
    typedef ap_int<10> acc_t;

    void fir1(data_t *y, data_t x);

    int main(){
        data_t x[11] = {0};
        data_t y;

        x[0] = 1; 

        for (int i=0; i<11; i++){
            fir1(&y, x[i]);
            printf("%d: y=%d\n",(int)i, (int)y);
        }
        
        
        return 0;
    }


.. note:: 

    In :numref:`fig_hls19`, we can see a significant reduction in the resource utilization after changing the bit-width to 10. 


.. _`fig_hls19`:

.. figure:: hls/hls19.png
   :width: 50%

   Bit-width = 10 (as min value is -512)


Sparse matrix multiplication
============================

Implementation
--------------

.. code-block:: cpp
    :linenos:
    :caption: Header filter

    // sm_mult.h

    #ifndef __SM_MULT_H__
    #define __SM_MULT_H__

    const static int SIZE = 4; // size of square matrix
    const static int NNZ = 9; // number of non zero elements
    const static int NUM_ROWS = 4; // same as SIZE

    typedef float D_TYPE;

    void sm_mult(int row_ptr[NUM_ROWS+1], int col_index[NNZ], 
        D_TYPE values[NNZ], D_TYPE y[SIZE], D_TYPE x[SIZE]);

    #endif


.. code-block:: cpp
    :linenos:
    :caption: Implementation of Sparse matrix multiplication

    // sm_mult.cpp

    #include "sm_mult.h"

    void sm_mult(int row_ptr[NUM_ROWS+1], int col_index[NNZ], 
        D_TYPE values[NNZ], D_TYPE y[SIZE], D_TYPE x[SIZE]){

        Loop1: for (int i=0; i<NUM_ROWS; i++){
            D_TYPE y0 = 0;

            Loop2: for (int k=row_ptr[i]; k<row_ptr[i+1]; k++){
                y0 += values[k] * x[col_index[k]];
            }
            y[i] = y0;
        }
    }


.. code-block:: cpp
    :linenos:
    :caption: Testbench for Sparse matrix multiplication

    // sm_mult_tb.cpp

    #include <stdio.h>
    #include "sm_mult.h"


    // function to generate output for comparision 
    // it's not an sparse multiplication method. 
    void matrix_vector(D_TYPE A[SIZE][SIZE], D_TYPE *y, D_TYPE *x){
        for (int i=0; i<SIZE; i++){
            D_TYPE y0 = 0;
            for (int j=0; j<SIZE; j++){
                y0 += A[i][j] * x[j];
                y[i] = y0;
            }
        }
    }


    int main(){
        int fail = 0;
        D_TYPE M[SIZE][SIZE] = {
                                {3, 4, 0, 0}, 
                                {0, 5, 9, 0}, 
                                {2, 0, 3, 1}, 
                                {0, 4, 0, 6}
                            };

        D_TYPE x[SIZE]={1, 2, 3, 4};
        D_TYPE y_sw[SIZE]; // store result of non-sparse multiplication

        // non zero values of matrix M
        D_TYPE values[] = {3, 4, 5, 9, 2, 3, 1, 4, 6};
        // location of non-zero elements
        int col_index[] = {0, 1, 1, 2, 0, 2, 3, 1, 3};
        // number of nonzero element till current row
        int row_ptr[] = {0, 2, 4, 7, 9};

        D_TYPE y[SIZE];

        // HLS calculation
        sm_mult(row_ptr, col_index, values, y, x);
        // software calculation
        matrix_vector(M, y_sw, x);

        for (int i=0; i<SIZE; i++){
            if (y_sw[i] != y[i]){
                fail = 1; 
                break;
            }
        }

        if (fail==1)
            printf("Failed\n");
        else
            printf("Passed\n");

        return fail; 
    }


.. note::

    * In :numref:`fig_hls20`, the latency is not defined as the number of interations are not constant-values in the 'for loop'. We can use the loop_tripcount directive to get an estimate of the latency. 

        .. code-block:: text
            
            # A, B and C are positive integers
            #pragma HLS loop_tripcount min=A max=B avg=C

    * Also, RTL cosimulation will give the value of 'latency' for a particular testbench as shown in :numref:`fig_hls21`. 
    

.. _`fig_hls20`:

.. figure:: hls/hls20.png
   :width: 50%

   loop_tripcount directive is required to get the latency values


.. _`fig_hls21`:

.. figure:: hls/hls21.png
   :width: 50%

   Latency calculation for the testbench data


Unroll and pipelining the inner loop
------------------------------------


.. note:: 

    * Unroll/pipelining directive should be apply to inner loop first. 
    * If we apply Unroll/pipelining directive to outer function, then it will try to unroll the inner loop as well which may lead to unoptimized design. 


.. code-block:: cpp
    :linenos:
    :caption: Unroll factor = 4

    // sm_mult.cpp

    #include "sm_mult.h"

    void sm_mult(int row_ptr[NUM_ROWS+1], int col_index[NNZ], 
        D_TYPE values[NNZ], D_TYPE y[SIZE], D_TYPE x[SIZE]){

        Loop1: for (int i=0; i<NUM_ROWS; i++){
            D_TYPE y0 = 0;

            Loop2: for (int k=row_ptr[i]; k<row_ptr[i+1]; k++){
    #pragma HLS PIPELINE
    #pragma HLS UNROLL factor=4
                y0 += values[k] * x[col_index[k]];
            }
            y[i] = y0;
        }
    }


* Better performance can be achieved with "unroll factor = 4"

.. _`fig_hls23`:

.. figure:: hls/hls23.png
   :width: 50%

   Unroll = 4 and pipelining the inner loop


* Performance will degrade with high unrolling. The unroll-values should be selected based on the code. 

.. _`fig_hls22`:

.. figure:: hls/hls22.png
   :width: 50%

   Unroll = 8 and pipelining the inner loop


