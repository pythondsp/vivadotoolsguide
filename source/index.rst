.. Vivado documentation master file, created by
   sphinx-quickstart on Mon Sep 17 12:50:27 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Vivado Tools
============

.. toctree::
    :maxdepth: 2
    :numbered:
    :includehidden:
    :caption: Contents:

    vivado/fft
    sysgen/sysgen
    hls/hls
    hls/hls2
    sdk/sdk
    mc/modelcomposer
    petalinux/petalinux
    sdsoc/sdsoc


.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
